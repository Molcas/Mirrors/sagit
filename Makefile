#
#
#
FC=gfortran
FCC=gcc
FCFLAGS=-O2
all: sagit
sagit: main.o blas.o drvmo.o getmem.o grid_util.o input_grid_it.o rdvec.o sagit.o seward_util.o shared_util.o util.o
	$(FC) $(FCFLAGS) -o $@ $^
main.o: main.c
	$(FCC) -c $(FCFLAGS) main.c
%.o: %.f
	$(FC) -c -fdefault-integer-8 $(FCFLAGS) $<
.PHONY: clean veryclean
clean:
	rm -f *.o
veryclean: clean
	rm -f sagit
