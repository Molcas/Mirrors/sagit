#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/***************************************************************/
/*
        S A G I T 
    stand alone Grid_IT code	

  The program is capable to use orbital file (v. 2.4) 
  from MOLCAS/OpenMolcas package and produce 
  file in LUSCUS format.
  
   For more information: www.molcas.org

   Authors: Roland Lindh (math part)
            Valera Veryazov (interface part)
   
*/   
/***************************************************************/



#define MYMAXPATH 512
#define INT long int     
#define MIN(x,y) (x<y? x : y)

void help(void);
void sagit_(char [], INT *, char [], INT *, char [], INT *);
void dis(void);
INT c_write_(INT *FileDescriptor,char *Buffer,INT *nBytes);

int main (int argc, char* argv[])
{
/* usage 
  sagit OrbFile
  sagit OrbFile -k keyfile -o Out.lus
  sagit OrbFile -p parameters
  sagit -h
*/
INT j,k,i;
INT keyfileset=0;
INT outfileset=0;
INT paramset=0;
INT orbfileset=0;
char keyfile[MYMAXPATH],outfile[MYMAXPATH],param[512];
char orbfile[MYMAXPATH],keys[MYMAXPATH];
FILE *f;
char *ptr;
INT io,ii,ik;

  if(argc==1) { help(); exit (0);}

  for(j=1;j<argc;j++)
     {
//       puts(argv[j]);
          if(argv[j][0]!='-')
                    {
                     strcpy(orbfile,argv[j]);
		     orbfileset=1;
                    }
          else
	  {
	   strcpy(keys,argv[j]+1);
	   switch(keys[0])
	    {
                  case '?':
                  case 'h':
                            help(); exit(0);
	     
	           case 'k': j++;
		        keyfileset=1;
		        strcpy(keyfile,argv[j]); break;
		   case 'o': j++;
		        outfileset=1;
		        strcpy(outfile,argv[j]); break;
		   case 'p': j++;
		        paramset=1;
		        strcpy(param,argv[j]); break;
		   case 'i': j++;
		        orbfileset=1;
		        strcpy(orbfile,argv[j]); break;
	           default: dis(); exit(0); 
	    }
           }
      }
 if(orbfileset==0)
   {
     strcpy(orbfile,"INPORB");
   }    
 if(outfileset==0) 
    {
       k=0;
       for(j=strlen(orbfile);j>=0;j--)
       {
         if(orbfile[j]=='.') {k=j; break;}
       }

       if(k>0) {
                strcpy (outfile,orbfile); 
		strcpy(outfile+k+1,"lus");
	       }
       else 
       { strcpy (outfile,orbfile);
       strcat(outfile,".lus");
       }
    }
  if(paramset==1)
    {
  /* make input.tmp file, dump parameters, and set key file */  
       strcpy(keyfile,"sagit.inp");
       keyfileset=1;
       f=fopen("sagit.inp","w");
       for(i=0;i<strlen(param);i++)
        {
           if(param[i]=='=') param[i]='\n';
           if(param[i]==';') param[i]='\n';
         }
       fprintf(f,"%s\n",param);
       fclose(f);
    }
   if(keyfileset==0)
     {
       strcpy(keyfile,"sagit.inp");
     } 
     ii=strlen(orbfile);
     io=strlen(outfile);
     ik=strlen(keyfile);
   sagit_(orbfile,&ii,outfile,&io,
         keyfile,&ik);
  return 0;
}

void help (void)
{
 printf(" Sagit is Stand Alone Grid IT program\n");
// printf("  (C) Valera Veryazov\n\n");
 
 printf("Usage:  sagit OrbFile\n  sagit OrbFile -k keyfile -o Out.lus\n  sagit OrbFile -p parameters \n  sagit -h\n  ");

exit;
}
void dis (void)
{
  puts("You have entered a keyword which program can not recognize");
  puts("May I suggest that you are looking for another sagit?");
  puts("\n\n");
  puts("For S�kring Av Gods I Trafik:              www.sagit.se");
  puts("For Sectoral Advisory Group on Intl Trade: www.international.gc.ca");
  puts("For South Australian Grain Industry Trust: sagit.com.au ");
  puts("For Sagit Agish -       en.wikipedia.org/wiki/Sagit_Agish");
  exit;
}
INT c_write_(FileDescriptor,Buffer,nBytes)
 INT *FileDescriptor;
 char *Buffer;
 INT *nBytes;

{
 INT rc=0;
 INT bfrblk=1024*1024;
 INT i=0;
 INT remains;
 INT writelength;
 remains=*nBytes;
 while (remains > 0){
      writelength = MIN(bfrblk,remains);
      rc = (INT)write(*FileDescriptor,(void *)(Buffer+i),(size_t)(writelength));
      if ( rc == writelength ) { i = i+writelength; rc = i; remains = remains - bfrblk;}
      else { rc = 0; return rc ;}
 }
 return rc;
}

INT prt_lusc_(INT *lid, char *line, INT *len, INT *isBin)
{
  INT marker[1];
  FILE *fp;
  int i;
  fp = (FILE*) *lid;

// printf("here %ld %ld \n", *len, *isBin );

  if(*isBin==1){
  marker[0]=*len;
  fwrite(marker,sizeof(INT),1, fp);
  }


  for (i = 0; i < *len; i++){ fputc(line[i], fp);}
  if(*isBin==1){
  marker[0]=*len;
  fwrite(marker,sizeof(INT),1, fp);
  }
  else
  {
  fputc(10, fp);
  }
  return 0;
}

INT lusopen_(INT *lid, char *fname, INT *fname_len)
{
  FILE *fp;
  char my[MYMAXPATH];
  strncpy(my,fname,MYMAXPATH-1);
// printf("in luopen %ld\n",*fname_len);
  my[*fname_len] = 0;

  fp = fopen(my, "wb");
  *lid = (INT) fp;
  return 0;
}
void dump_lusc_(INT *lid, double *Buff, INT *nBuff)
{
 FILE *fp;
  fp = (FILE*) *lid;
  fwrite(Buff,sizeof(double),*nBuff,fp);

 return;
}



