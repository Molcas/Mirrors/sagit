      subroutine SAGIT(orbf_c,iorbf,outf_c,ioutf,keyf_c,ikeyf)
      implicit real*8 (a-h,o-z)
      Character*512 INPORB
      character*512 orbf,outf,keyf
      character*512 orbf_c,outf_c,keyf_c
        if(iorbf.gt.512.or.iorbf.lt.0) then
          write(6,*) 'something is wrong'
          write(6,*) 'too long filename or bitness problem'
          stop
        endif
      orbf=' '
      orbf(1:iorbf)=orbf_c(1:iorbf)
      outf=' '
      outf(1:ioutf)=outf_c(1:ioutf)
      keyf=' '
      keyf(1:ikeyf)=keyf_c(1:ikeyf)
c input file
      close(5)
      open(5,file=keyf)
      

      Write(6,*) 'SAGIT - stand alone grit_it'
      Write(6,*)
      Write(6,*) 'InpOrb: ',orbf(1:iorbf)
      Write(6,*) 'input   ',keyf(1:ikeyf)
      Write(6,*) 'output  ',outf(1:ioutf)

      Call inimem_

      INPORB=orbf(1:iorbf)

      Call Seward_Init
*
      Call GetInf(Info,nInfo,orbf,iorbf)

*
      Call Input_Grid_It(1,INPORB,0,outf)
*
*
      Call DrvMO(1,INPORB)

      ireturn=0
      stop 0

      end
