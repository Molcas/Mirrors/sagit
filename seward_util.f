c
c
c  Note that this is a very dirty file, should be cleaned 
c
c
c
c



      Subroutine Gen_RelPointers(ibase)
      Implicit Real*8 (A-H,O-Z)
      include "info.fh"
*
      Do i = 1, Mx_Shll
         ipCff(i)        = ipCff(i)         + ibase
c         ipCff_Cntrct(i) = ipCff_Cntrct(i)  + ibase
c         ipCff_Prim(i)   = ipCff_Prim(i)    + ibase
         ipExp(i)        = ipExp(i)         + ibase
c         ipBk(i)         = ipBk(i)          + ibase
c         ip_Occ(i)       = ip_Occ(i)        + ibase
c         ipAkl(i)        = ipAkl(i)         + ibase
c         ipFockOp(i)     = ipFockOp(i)      + ibase
      End Do
      Do i = 1, Mxdbsc
         ipCntr(i)     = ipCntr(i)     + ibase
      End Do
c      ipEF = ipEF + ibase
c      ipOAM= ipOAM+ ibase
c      ipOMQ= ipOMQ+ ibase
c      ipDMS= ipDMS+ ibase
c      ipWel= ipWel+ ibase
c      ipXF = ipXF + ibase
c      ipAMP= ipAMP+ ibase
c      ipRP1=ipRP1 + ibase
c      ipXMolnr=ipXMolnr + ibase
c      ipXEle=ipXEle + ibase
*
*
      Return
      End
      SubRoutine GetInf(Info,nInfo,orbf,iorbf)
************************************************************************
*                                                                      *
* Object: to read all input information on the file INFO.              *
*                                                                      *
*                                                                      *
*     Author: Roland Lindh, Dept. of Theoretical Chemistry,            *
*             University of Lund, SWEDEN                               *
*             January 1992                                             *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
      include "info.fh"
      include "WrkSpc.inc"
      Integer ipSph(0:MxAng)
      Common /Sph/ ipSph

       character Str*120
       character orbf*(*)


      open(16, file=orbf(1:iorbf))
1     read(16,'(a)',end=200,err=200) Str
      if(Str(1:6).eq.'#BASIS') goto 10
      goto 1
200   write(6,*) "BASIS section is not found"
      return
10    continue            
*
*
      read(16,*) Len
c       print *,'Garbage array SewIInfo=', Len

      call GETMEM('SewIInfo','ALLO','INTE',ii,Len)
      call unpack_me(16,iWORK(ii),Len)
      call push_iinfo(iWORK(ii),ixStrt,Len)
      call GETMEM('SewIInfo','FREE','INTE',ii,Len)
      
       
       read(16,*) iTemp
c       print *,'Garbage array VVInfo=', iTemp

       call GETMEM('VVInfo','ALLO','INTE',iivv,iTemp)
       call unpack_me(16,iWORK(iivv),iTemp)
       icurr=0
       Mx_Shll=iWORK(iivv+icurr)
       icurr=icurr+1

      call irevcopy(Mx_Shll,nExp,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Shll
      call irevcopy(Mx_Shll,nBasis,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Shll
      call irevcopy(Mx_Shll,nBasis_Cntrct,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Shll

      call irevcopy(Mx_Shll,ipCff,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Shll
c      call irevcopy(Mx_Shll,ipCff_Cntrct,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Shll
c      call irevcopy(Mx_Shll,ipCff_Prim,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Shll
      call irevcopy(Mx_Shll,ipExp,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Shll
      nShlls=iWORK(iivv+Icurr)
      Icurr=iCurr+1
      call irevcopy(nShlls,IndS,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+nShlls
      
      nCnttp=iWORK(iivv+Icurr)
      Icurr=iCurr+1
      call irevcopy(nCnttp,nOpt,1,iWORK(iivv+Icurr),1)
      Icurr=iCurr+nCnttp
      ii=iWORK(iivv+Icurr)
      Icurr=iCurr+1
      call irevcopy(64*Mx_mdc,iCoset,1,iWORK(iivv+Icurr),1)
      Icurr=iCurr+64*Mx_mdc

      
      Call ICopy(MxUnq,1,0,IrrCmp,1)
c 
      Mx_Unq=iWORK(iivv+Icurr)
      Icurr=iCurr+1
      call irevcopy(Mx_Unq,IrrCmp,1,iWORK(iivv+Icurr),1) 
      Icurr=iCurr+Mx_Unq
      
*
      Call GETMEM('AS','ALLO','INTE',ip_AS,8*Mx_AO)
      ii=iWORK(iivv+Icurr)
      Icurr=iCurr+1
      call irevcopy(8*Mx_AO,iWORK(ip_AS),1,iWORK(iivv+Icurr),1)
      Icurr=iCurr+8*Mx_AO
      
      jp_AS = ip_AS
      Do i = 1, Mx_AO
         Call ICopy(8,iWORK(jp_AS),1,iAOtSO(i,0),MxAO)
         jp_AS = jp_AS + 8
      End Do
      Call GETMEM('AS','FREE','INTE',ip_AS,8*Mx_AO)
*
      LenL=iWORK(iivv+Icurr)
      Icurr=iCurr+1

      do i=1,LenL
        ii=iWORK(iivv+Icurr+i-1)
        if(ii.eq.0) Prjct(i)=.false. 
        if(ii.eq.1) Prjct(i)=.true. 
      enddo
      Icurr=iCurr+LenL
      
      do i=1,LenL
        ii=iWORK(iivv+Icurr+i-1)
        if(ii.eq.0) Transf(i)=.false. 
        if(ii.eq.1) Transf(i)=.true. 
      enddo
      Icurr=iCurr+LenL

      do i=1,LenL
        ii=iWORK(iivv+Icurr+i-1)
        if(ii.eq.0) AuxShell(i)=.false. 
        if(ii.eq.1) AuxShell(i)=.true. 
c        print *,'AuxS',AuxShell(i)
      enddo
      Icurr=iCurr+LenL

      do i=1,LenL
        ii=iWORK(iivv+Icurr+i-1)
        if(ii.eq.0) AuxCnttp(i)=.false. 
        if(ii.eq.1) AuxCnttp(i)=.true. 
c        print *,'AuxC',AuxCnttp(i)
      enddo
      Icurr=iCurr+LenL

*
      Call GETMEM('VVInfo','FREE','INTE',iivv,Itemp)
      
      
      read(16,*) Len
c       print *,'Garbage array SewRInfo=', Len

      call GETMEM('SewRInfo','ALLO','REAL',ii,Len)

      call unpack_meR(16,WORK(ii),Len)
      call push_rinfo(WORK(ii),rxStrt,Len)
     
      call GETMEM('SewRInfo','FREE','REAL',ii,Len)

*
      read(16,*) Len
c      write(6,*) 'Len SewXInfo=',Len
      nInfo=Len
      LenInf=nInfo
      Call GETMEM(' SewXInfo ','ALLO','REAL',Info,nInfo)
      call unpack_mer(16, WORK(Info),Len)

*
      iOld = LctInf

      LctInf=Info
      Call Gen_RelPointers(LctInf-1)
*

         read(16,*) Len2
c         print *,'Garbage arrays SewTInfo =',Len2
         Call GETMEM(' Sphere','ALLO','REAL',ipRph,Len2)

         Do 2 iAng = 0, iAngMx-1
            ipSph(iAng+1)= ipSph(iAng) + (iAng*(iAng+1)/2 + iAng + 1)**2
c          print *,'ipSph in init',ipSph(iAng+1)
 2       Continue
         call unpack_mer(16,WORK(ipRph),Len2)
         
c         Call GETMEM(' Sphere','FREE','REAL',ipSph(0),Len2)
         
      close(16)
      Return
      End
************************************************************************
      Subroutine Seward_Init
************************************************************************
*                                                                      *
*     Object: to set data which is stored in common blocks             *
*                                                                      *
*     Author: Roland Lindh, IBM Almaden Research Center, San Jose      *
*             January '90                                              *
************************************************************************
      implicit real*8 (a-h,o-z)
       include "info.fh"
*

       include "WrkSpc.inc"
*                                                                      *
************************************************************************
*                                                                      *
C
C...  Parameters for srint
C
c      shortrange=.False.
c      isr_simulate=0
*
*     Initialize FMM.fh
*
*                                                                      *
************************************************************************
*                                                                      *
*
*-----Info
*
c     data iPhase/ 1, 1, 1,   -1, 1, 1,   1,-1, 1,  -1,-1, 1,
c    &             1, 1,-1,   -1, 1,-1,   1,-1,-1,  -1,-1,-1/
c      Seward_Status=InActive
      do j=0,4
      do i=1,3
        iPhase(i,j)=1
      enddo
      enddo
      iPhase(1,1)=-1
      iPhase(2,2)=-1
      iPhase(1,3)=-1
      iPhase(2,3)=-1
      iPhase(3,4)=-1
      do j=5,7
      do i=1,3
        iPhase(i,j)=-1
      enddo
      enddo
      iPhase(2,5)=1
      iPhase(1,6)=1
*
*     Info
*
      nCnttp=0
      iAngMx=-1
      Call IZero(iChCar,3)
      do i=1,Mxdbsc
        lOffAO(i)=0
      enddo
      do i=1,Mxdbsc
      do j=0,MxAng
         kOffAO(i,j)=0
      enddo
      enddo
      do i=1,MxAO
      do j=0,7
        iAOtSO(i,j)=-999999999
      enddo
      enddo
      do i=0,MxAng
        MaxBas(i)=0
       MaxPrm(i)=0
      enddo
      do i=1,MxUnq
        IrrCmp(i)=0
      enddo

      Do i=1, Mxdbsc
	ipCntr(i)=ip_Dummy
      End Do
*
*-----LInfo
*
      Do i=1,Mx_Shll
         Prjct(i)=.True.
         Transf(i)=.True.
         AuxShell(i)=.False.
         nExp(i)=0
         nBasis(i)=0
         ipExp(i)=ip_Dummy
         ipCff(i)=ip_Dummy
      End Do
*
*
      Return
      End
