
      SubRoutine DrvMO(iRun,INPORB)
************************************************************************
*                                                                      *
* Object:                                                              *
*                                                                      *
*     Author: Roland Lindh, Dept. of Theoretical Chemistry,            *
*             Valera Veryazov, Dept. Theoretical Chemistry             *
*                                                                      *
*                                                                      *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
      include "info.fh"
      include "WrkSpc.inc"
      include "grid.fh"

c      Logical Debug
      Character str*128, Crypt*7
      Character INPORB*(*)
      Character*80 myTitle
      character*128 line
      Real*8 pp(3)
      Integer nTypes(7)
      integer nSym
      Data Crypt/'fi123sd'/
*
*---- Set size of batches
*
*
      parameter (nIncPack=18*1024)

      Character cMoBlock(nIncPack*2)

      nInc=nIncPack

*                                                                      *
************************************************************************
*
*...  Prologue
c      Debug=.false.
      isEner=1
      iRout = 2
c      iPack=1
       ipCutOff=ip_iDummy

      dNorm=0
      ddNorm=0
       Write (6,*)
       Write (6,'(A,8I5)') 'Irreps  : ',(i,i=1,nIrrep)
       Write (6,'(A,8I5)') 'Basis   : ',(nBas(i),i=0,nIrrep-1)
       Write (6,'(A,3I5)') 'Grid Net: ',(iGridNpt(i),i=1,3)
       Write (6,*)
*
*...  Compute the size of the densities
*
      nCMO=0
      nMOs=0
      Do iIrrep = 0, nIrrep - 1
         nCMO  = nCMO + nBas(iIrrep)**2
         nMOs  = nMOs + nBas(iIrrep)
      End Do
*

      if(isUHF.ne.0.and.(isDerivative.ne.0.or.isCurDens.ne.0)) then
        write(6,*) "ERROR - Current density or derivatives not
     &              implemented for UHF!!!"
        call abend_q
      end if
      if(NoOrb.ne.0.and.(isDerivative.ne.0.or.isCurDens.ne.0)) then
        write(6,*) "ERROR - Current density or derivatives not
     &              implemented with the NOORB keyword!!!"
        call abend_q
      end if
      if(isAtom.ne.0.and.(isDerivative.ne.0.or.isCurDens.ne.0)) then
        write(6,*) "ERROR - Current density or derivatives not
     &              implemented with the ATOM keyword!!!"
        call abend_q
      end if


        Call GETMEM('CMO','ALLO','REAL',ipCMO,nCMO)

      Call GETMEM('Ener','ALLO','REAL',ipE,nMOs)
      Call GETMEM('Occu','ALLO','REAL',ipOcc,nMOs)
      Call GETMEM('Occ2','ALLO','REAL',ipOoo,nMOs)
      if(isVirt.eq.1) then
        Call GETMEM('ddNo','ALLO','REAL',ipdd,nMOs*nMOs)
        Do i=1,nMOs*nMOs
         WORK(ipdd+i-1)=0.0d0
        Enddo
      endif
      Call GETMEM('iTyp','ALLO','INTE',ipType,nMOs)
      Call GETMEM('Vol','ALLO','REAL',ipVol,nMOs)
      Call GETMEM('Sort','ALLO','INTE',ipSort,nMOs)
      Call GETMEM('Nzer','ALLO','INTE',ipNZ,nMOs*2)
      Call GETMEM('NRef','ALLO','INTE',ipGRef,nMOs)
      Call GETMEM('DoIt','ALLO','INTE',ipDoIt,nMOs)
      Call GETMEM('Pab','ALLO','REAL',ipPab,nMOs)
        Do i=0, nMOs-1
         iWORK(ipGRef+i)=-1
        enddo
*
*  Read information from INPORB file
*
c       LuOrb=isFreeUnit(46)
       LuOrb=56


      if(isUHF.eq.0) then

        Call RdVec(INPORB,LuOrb,'COE',nIrrep,NBAS,NBAS,
     &    WORK(ipCMO),WORK(ipOcc),WORK(ipE),iDummy,
     &    myTitle,0,iErr)


c construct Pab
       if(NoOrb.eq.1) then
c        print *,'nCMO,nMOs', nCMO,nMOs
        Call makePab(WORK(ipCMO),WORK(ipOcc),WORK(ipPab),nMOs,nMOs,
     &  nIrrep, nBas    )
c       print *,'Pab=', (WORK(ipPab+i),i=0,nMOs-1)
       endif
        if(iErr.eq.1) then
          do j=0, nMOs-1
          WORK(ipE+j)=0.0d0
          enddo
        endif
      Call RdVec(INPORB,LuOrb,'I',nIrrep,NBAS,NBAS,
     &  Dummy,Dummy,Dummy,iWORK(ipType),
     &  myTitle,0,iErr)
        if(iErr.eq.1) then
          do j=0, nMOs-1
          iWORK(ipType+j)=0
          enddo
        endif
      else  ! UHF case
c allocate memory for extra arrays.
      Call GETMEM('CMO_ab','ALLO','REAL',ipCMO_ab,nCMO)
      Call GETMEM('Ener_ab','ALLO','REAL',ipE_ab,nMOs)
      Call GETMEM('Occu_ab','ALLO','REAL',ipOcc_ab,nMOs)
      Call GETMEM('Sort_ab','ALLO','INTE',ipSort_ab,nMOs)
      Call GETMEM('NRef_ab','ALLO','INTE',ipGRef_ab,nMOs)
      Call GETMEM('DoIt_ab','ALLO','INTE',ipDoIt_ab,nMOs)

      Call RdVec_(INPORB,LuOrb,'COE',1,nIrrep,NBAS,NBAS,
     &  WORK(ipCMO),WORK(ipCMO_ab),WORK(ipOcc),WORK(ipOcc_ab),
     &  WORK(ipE),WORK(ipE_ab),iDummy,
     &  myTitle,0,iErr,iWFtype)
c it can be only after SCF, so we do not need TypeIndex info
          do j=0, nMOs-1
          iWORK(ipType+j)=0
          enddo

      endif
      do j=1,7
      nTypes(j)=0
      enddo
      do j=0, nMOs-1
      jj=iWORK(ipType+j)
      if(jj.gt.0) nTypes(jj)=nTypes(jj)+1
      enddo

      close(LuOrb)
*
*  Calculate net.
*
       if(iRun.eq.1) then
        Write(6,'(A)') '   Input vectors read from INPORB'
        Write(6,'(A,A)') '   Orbital file label: ',
     &                   myTitle(:mylen(myTitle))
       endif
      do j=1,3
       iGridNpt(j)=iGridNpt(j)+1
      enddo

      nCoor=iGridNpt(1)*iGridNpt(2)*iGridNpt(3)
      iiCoord=nCoor
*
      if(isTheOne.eq.1) nCoor=int(OneCoor(7)+0.3)
      if(isAtom.eq.1) nCoor=nAtoms
      if(isUserGrid.eq.1) nCoor=nGridPoints
      Write (6,*) ' Number of grid points in file:  ',nCoor
c      call iXML('nPoints',nCoor)

*************************************************************************
* And now we had to choose orbitals to draw.
*
* Sometime we had to make an automatic guess....
*
*************************************************************************

       Call PickOrb(ipNz,ipSort,ipGref,ipSort_ab,
     &  ipGref_ab,ipVol,ipE,ipOcc,ipE_ab,ipOcc_ab,
     &  nShowMOs,nShowMOs_ab,isener,nMOs,myTitle,ipType)
*                                                                      *
*---- Start run over sets of grid points
*
c       if(isBinary.eq.1) then
       nShowMOs=nShowMOs+isDensity+isSphere+isColor
c       else
c       nShowMOs=1
c       endif
      if(isUHF.eq.1) nShowMOs_ab=nShowMOs_ab+isDensity+isSphere+isColor
      nShowMOs2=nShowMOs+nShowMOs_ab
       Write (6,*)
       Write (6,*) ' Total number of MOs               :',nMOs
c       call iXML('nMOs',nMOs)
       Write (6,*) ' Number MOs for grid               :',nShowMOs2
       Write (6,*) ' Batches processed in increments of:',nInc
       Write (6,*)
      iPrintCount=0

        Call GETMEM('MOValue','ALLO','REAL',ipMO,nInc*nMOs)
        Call GETMEM('DOValue','ALLO','REAL',ipOut,nInc)


      if (imoPack .ne. 0) then
        Call GETMEM('PackedBlock','ALLO','INTE',ipPBlock,nInc)
      else
        Call GETMEM('PackedBlock','ALLO','INTE',ipPBlock,1)
        iWORK(ipPBlock)=0
      endif

*
*.... Allocate memory for the some grid points
*
      Call GETMEM('Coor','ALLO','REAL',ipC,nInc*3)
      iSphrDist =ip_Dummy
      iSphrColor=ip_Dummy

      if(isSphere.eq.1) then
      Call GETMEM('SpDi','ALLO','REAL',iSphrDist,nInc)
      endif
      if(isColor.eq.1) then
      Call GETMEM('SpCo','ALLO','REAL',iSphrColor,nInc)
      endif

*
*   check grids to calculate
*
      Do i=0, nMOs-1
        iWORK(ipDoIt+i)=0
        if(isUHF.eq.1) iWORK(ipDoIt_ab+i)=0
      enddo
c
      if(NoOrb.eq.1) goto 550
      Do i=1,nShowMOs-isDensity-isSphere-isColor
        iWORK(ipDoIt+iWORK(ipGRef+i-1)-1)=1
      enddo
      if(isUHF.eq.1) then
      Do i=1,nShowMOs_ab-isDensity-isSphere-isColor
        iWORK(ipDoIt_ab+iWORK(ipGRef_ab+i-1)-1)=1
      enddo
      endif
      ifpartial=1-isTotal
      if(isTotal.eq.1) then
        Do i=0, nMOs-1
          if(abs(WORK(ipOcc+i)).gt.0.0d0) then
                     iWORK(ipDoIt+i)=1
          endif
          if(isUHF.eq.1) then
          if(abs(WORK(ipOcc_ab+i)).gt.0.0d0) then
                     iWORK(ipDoIt_ab+i)=1
          endif
          endif
        enddo
      else
        Do i=0, nMOs-1
          if(WORK(ipOcc+i).gt.0.0d0.and.iWORK(ipDoIt+i).ne.1) then
                     ifpartial=1
          endif
          if(isUHF.eq.1) then
          if(WORK(ipOcc_ab+i).gt.0.0d0.and.iWORK(ipDoIt_ab+i).ne.1) then
                     ifpartial=1
          endif
          endif
        enddo
      endif
550   Continue
*
*   If plotting VB orbitals : count active orbitals and make sure
*   all are included in DOIT :
*
* nothing to do with UHF

c
      iCRSIZE=1
      NBYTES=10
      NINLINE=10
c       Print *, 'prepear  header '

      call PrintHeader(nMOs,nShowMOs,nShowMOs_ab,nCoor,nInc,
     & iiCoord,nTypes,iCRSIZE,NBYTES,NINLINE,nBlocks )

c       Print *, 'HERE header isdone'

      if(isLuscus.eq.1) LuVal=LID
      Call PrintTitles(LuVal,nShowMOs,isDensity,nMOs,
     &  iWORK(ipGRef), isEner,  WORK(ipOcc), iWORK(ipType), Crypt,
     &  iWORK(ipNZ), WORK(ipE), VBocc, ifpartial,isLine,isSphere,
     &  isColor, ISLUSCUS,ncoor,nBlocks,nInc)
      if(isUHF.eq.1) then
      if(isLuscus.eq.1) LuVal_ab=LID_ab
      Call PrintTitles(LuVal_ab,nShowMOs_ab,isDensity,nMOs,
     &  iWORK(ipGRef_ab), isEner,  WORK(ipOcc_ab),
     &  iWORK(ipType), Crypt,
     &  iWORK(ipNZ), WORK(ipE_ab), VBocc, ifpartial,isLine,isSphere,
     &  isColor, ISLUSCUS,ncoor,nBlocks,nInc)
      endif
*                                                                      *

*                                                                      *
************************************************************************
*
*---- Loop over grid points in batches of nInc
*
      ive3=max(iGridNpt(3)-1,1)
      ive2=max(iGridNpt(2)-1,1)
      ive1=max(iGridNpt(1)-1,1)
       iv3=0
       iv2=0
       iv1=0
c      if(isAtom.eq.1) goto 6000

      iiiCoord=0
      dtot=0.d0
c      if(isCutOff.eq.1) nCoor=iiCoord
cccccccccccccc  main loop starts here  ccccccccccccccccccccccccccccccccc
      iShiftCut=0
      Do iSec = 1, nCoor, nInc
      mCoor=Min(nInc,nCoor-iSec+1)
c      write(status,'(a,i8,a,i8)') ' batch ',iSec,' out of ',nCoor/nInc
c      call StatusLine('grid_it: ',status)
* Generate next portion of points..
      if (isTheOne.eq.0) then
c general case: we have a CUBIC box.
       ipPO=0
667    continue
       iiiCoord=iiiCoord+1
        gv3=1.0d+0*iv3/ive3
        gv2=1.0d+0*iv2/ive2
        gv1=1.0d+0*iv1/ive1

        if(isUserGrid.eq.0) then
          if(isCutOff.eq.0) then
            WORK(ipC+ipPO*3)=GridOrigin(1)+
     *        GridAxis1(1)*gv1+GridAxis2(1)*gv2+GridAxis3(1)*gv3
            WORK(ipC+ipPO*3+1)=GridOrigin(2)+
     *        GridAxis1(2)*gv1+GridAxis2(2)*gv2+GridAxis3(2)*gv3
            WORK(ipC+ipPO*3+2)=GridOrigin(3)+
     *        GridAxis1(3)*gv1+GridAxis2(3)*gv2+GridAxis3(3)*gv3
          else
c using ipCutOff
            WORK(ipC+ipPO*3)=40
            WORK(ipC+ipPO*3+1)=40
            WORK(ipC+ipPO*3+2)=40
             if(iWORK(ipCutOff+iiiCoord-1).eq.1) then
              WORK(ipC+ipPO*3)=GridOrigin(1)+
     *           GridAxis1(1)*gv1+GridAxis2(1)*gv2+GridAxis3(1)*gv3
              WORK(ipC+ipPO*3+1)=GridOrigin(2)+
     *           GridAxis1(2)*gv1+GridAxis2(2)*gv2+GridAxis3(2)*gv3
              WORK(ipC+ipPO*3+2)=GridOrigin(3)+
     *           GridAxis1(3)*gv1+GridAxis2(3)*gv2+GridAxis3(3)*gv3
              endif
          endif
        else
          WORK(ipC+ipPO*3+1-1)=WORK(ipGrid+(iSec+ipPO-1)*3+1-1)
          WORK(ipC+ipPO*3+2-1)=WORK(ipGrid+(iSec+ipPO-1)*3+2-1)
          WORK(ipC+ipPO*3+3-1)=WORK(ipGrid+(iSec+ipPO-1)*3+3-1)
          !make a local copy of the weights of the corresponding grid points:
        endif
        iv3=iv3+1
        if (iv3.gt.ive3) then
           iv3=0
           iv2=iv2+1
        endif
        if (iv2.gt.ive2) then
           iv2=0
           iv1=iv1+1
        endif
        ipPO=ipPO+1
c           print *,'ipo',ipP0
            if(ipPO.le.mCoor-1) goto 667
C end of CUBIC box
      else
C coords are given in specific points
* coords for DEBUG mode
       if(isLine.eq.0) then
         do i=0,nCoor-1
           WORK(ipC+i*3)=OneCoor(1)+OneCoor(4)*i
           WORK(ipC+1+i*3)=OneCoor(2)+OneCoor(5)*i
           WORK(ipC+2+i*3)=OneCoor(3)+OneCoor(6)*i
         enddo
       else
c LINE keyword
         do i=0,nCoor-1
            WORK(ipC+i*3)=
     &        OneCoor(1)+(OneCoor(4)-OneCoor(1))*i/(nCoor-1)
            WORK(ipC+1+i*3)=
     &        OneCoor(2)+(OneCoor(5)-OneCoor(2))*i/(nCoor-1)
            WORK(ipC+2+i*3)=
     &        OneCoor(3)+(OneCoor(6)-OneCoor(3))*i/(nCoor-1)
         enddo
       endif
      endif
C end of coordinates.
CVV: FIXME; separate color and sphere
C      if(isSphere.eq.1.and.isColor.eq.1) then
C        Call Sphr_Grid(WORK(ipCoor),mCoor,WORK(ipC),WORK(iSphrDist),
C     &       WORK(iSphrColor))
C      endif
c        print *,'VV',Work(ipC),Work(ipC+1),Work(ipC+2)
        if(NoOrb.eq.1) then
        nDrv=0
        Call MOEval(WORK(ipMO),nMOs,mCoor,WORK(ipC),WORK(ipPab),nMOs,
     &              iWORK(ipDoIt),nDrv,1)
        else
           nDrv=0
        Call MOEval(WORK(ipMO),nMOs,mCoor,WORK(ipC),WORK(ipCMO),nCMO,
     &                 iWORK(ipDoIt),nDrv,1)
        endif
*
*....... Write out values
*
       nLine=min(nShowMOs,20)
       nSLine=nLine*mCoor
       if(isLine.eq.0) then
         nSLine=1
         nLine=1
        endif
      Call GETMEM('Line','ALLO','REAL',ipLine,nSLine)
       if(levelprint.lt.2) iPrintCount=100
CVV BUG Update ipcutOFF
c         print *,'vv',Work(ipMO),work(ipMO+1)
        Call DumpM2Msi(iRun,Luval,LID,nShowMOs,isDensity,nMOs,
     &    iWORK(ipGRef), WORK(ipOcc), WORK(ipMO),
     &    WORK(ipOut), mCoor,
     &     iGauss, nInc, imoPack, iWORK(ipPBlock),
     &    cMoBlock,nBytesPackedVal, dnorm, Crypt, VbOcc,
     &    isTheOne,isLine,isBinary, isEner,iWORK(ipType),
     &    iWORK(ipNZ),WORK(ipE),WORK(ipLine),nLine,WORK(ipC),
     &    iPrintCount,isDebug,isCutOff,iWORK(ipCutOff+iShiftCut),
     &    isSphere,WORK(iSphrDist),isColor,WORK(iSphrColor),
     &    isLuscus,NBYTES,NINLINE)
c
c      if(isXField.eq.1) call dcopy_(mCoor,WORK(ipOut),1,WORK(ipOutXF),1)
        if(isUHF.eq.1) then
cVV:
          nDrv=0
          Call MOEval(WORK(ipMO),nMOs,mCoor,WORK(ipC),WORK(ipCMO_ab),
     &                nCMO,iWORK(ipDoIt_ab),nDrv,1)

*....... Write out values
*
          Call DumpM2Msi(iRun,Luval_ab,LID_ab,nShowMOs_ab,isDensity,
     &      nMOs,
     &      iWORK(ipGRef_ab), WORK(ipOcc_ab), WORK(ipMO),
     &      WORK(ipOut), mCoor,
     &       iGauss, nInc, imoPack, iWORK(ipPBlock),
     &      cMoBlock,nBytesPackedVal, dnorm, Crypt, VbOcc,
     &      isTheOne,isLine,isBinary, isEner,iWORK(ipType),
     &      iWORK(ipNZ),WORK(ipE_ab),WORK(ipLine),nLine,WORK(ipC),
     &      iPrintCount,isDebug,isCutOff,iWORK(ipCutOff+iShiftCut),
     &      isSphere,WORK(iSphrDist),isColor,WORK(iSphrColor),
     &      isLuscus,NBYTES,NINLINE)

        endif ! end if(isUHF.eq.1)


c

       Call GETMEM('Line','FREE','REAL',ipLine,nSLine)
        iShiftCut=iShiftCut+mCoor
       if(isVirt.eq.1) then
         do iiMO=1,nMOs
         do jjMO=1,nMOs
         if(WORK(ipOcc+iiMO-1).gt.1.1
     &      .and. WORK(ipOcc+jjMO-1).lt.0.9) then
c  here if this is a pair Occ-Virt

          do i=1,nMOs
          WORK(ipOoo+i-1)=0
          if(i.eq.iiMO) WORK(ipOoo+i-1) = 2.0d0-Virt
          if(i.eq.jjMO) WORK(ipOoo+i-1) = Virt
          enddo

          call outmo(0,2,WORK(ipMO),WORK(ipOoo),WORK(ipOut),
     >               mCoor,nMOs)
          dd=0
          do j=1, mCoor
            dd=dd+WORK(ipOut+j-1)
          enddo
c         ddNorm=ddNorm+dd
          call save_ddNorm(dd,iiMO,jjMO,WORK(ipdd),nMOs)
         endif
         enddo
         enddo
        endif
       Enddo !iSec
cccccccccccccc  main loop ends here  ccccccccccccccccccccccccccccccccc


      Write (6,*)
* Check norms
*

      if(isAtom.ne.1) then

        det3=GridAxis1(1)*(GridAxis2(2)*GridAxis3(3)-
     -                     GridAxis2(3)*GridAxis3(2))-
     -        GridAxis1(2)*(GridAxis2(1)*GridAxis3(3)-
     -                     GridAxis2(3)*GridAxis3(1))+
     +        GridAxis1(3)*(GridAxis2(1)*GridAxis3(2)-
     -                     GridAxis2(2)*GridAxis3(1))
        det3=det3/( (iGridNpt(1)-1)*(iGridNpt(2)-1)*(iGridNpt(3)-1))
        dNorm=dNorm*det3

c        print *,'dNorm=',dNorm

       if(isVirt.eq.1) then
        call print_ddNorm(nMOs,WORK(ipdd),det3)

c       print *,'ddNorm=',ddNorm*det3

       endif

c        Call Add_Info('GRIDIT_NORM',dNorm,1,6)
      endif
*                                                                      *
************************************************************************
*                                                                      *
**      Call DAClos(LuVal)
        IF (ISLUSCUS .EQ. 1) THEN
            LINE=' '
            CALL PRINTLINE(LUVAL,LINE,1,0)
            LINE=' </DENSITY>'
            CALL PRINTLINE(LUVAL,LINE,11,0)
            LINE=' <BASIS>'
            CALL PRINTLINE(LUVAL,LINE,8,0)
            LINE=' </BASIS>'
            CALL PRINTLINE(LUVAL,LINE,9,0)
            LINE=' <INPORB>'
            CALL PRINTLINE(LUVAL,LINE,9,0)
          endif


      if(isLine.eq.0) then
      write(str,'(9i8)') nIrrep, (nBas(i),i=0,nIrrep-1)
      CALL PrintLine(luval, STR, 72,0)
C      if(isBinary.eq.0) Then
C        write(LuVal,'(a)') str
C      else
C        write(LuVal) str
C      endif
c Well, to avoid rewritting of Cerius2 we use old INPORB format
c temporary!

c      LuOrb=isFreeUnit(46)
      LuOrb=66
c      call molcas_open_ext2(luorb,INPORB,'sequential','formatted',
c    & iostat,.false.,irecl,'old',is_error)
      Open(Unit=LuOrb, File=INPORB, status='old')
c 4001  read(LuOrb,'(a)',err=5000,end=5000) str
c      if(str.ne.'#ORB') goto 4001
5001  read(LuOrb,'(a)',err=5000,end=5000) str
c      if(str(1:1).eq.'#') goto 5001
      iLen=len(str)
      do 5050 i=iLen,1,-1
        if(str(i:i).ne.' ') goto 5060
5050  continue
 5060 CONTINUE
      CALL PrintLine(luval, STR, I,0)
C5060  if(isBinary.eq.0) Then
C        write(LuVal,'(a)') str(1:i)
C      else
C        write(LuVal) str(1:i)
C      endif
      goto 5001
5000  close(unit=LuOrb)
      endif ! isLine
      close(unit=LuVal)
      if(isUHF.eq.1) close(unit=LuVal_ab)
      if(isTheOne.eq.1) then
        MM=mCoor-1
        if(MM.gt.10) MM=10
c        Call Add_Info('GRIDIT_ONE',WORK(ipOut),MM,6)
      endif

*                                                                      *
************************************************************************
*                                                                      *
*...  Epilogue, end
*
c6000  continue

      if(isAtom.eq.1)  then
        mCoor=nCoor
        nDrv=0
        Call MOEval(WORK(ipMO),nMOs,mCoor,WORK(ipCoor),WORK(ipCMO),
     &             nCMO,iWORK(ipDoIt),nDrv,1)
        call outmo(0,2,WORK(ipMO),WORK(ipOcc),WORK(ipOut),nCoor,nMOs)
        write(6,'(60a1)') ('*',i=1,60)
      if(ifpartial.eq.0) Then
       write(6,'(a5,3a10,a20)') 'Atom','x','y','z','Density'
      else
       write(6,'(a5,3a10,a20)') 'Atom','x','y','z','Density (partial)'
      endif
          do i=0, nAtoms-1
           write(6,'(a5,3f10.3,e20.10)') AtomLbl(i+1),WORK(ipCoor+i*3),
     &   WORK(ipCoor+i*3+1),WORK(ipCoor+i*3+2),WORK(ipOut+i)
          enddo
c        Call Add_Info('GRIDIT_ATOM',WORK(ipOut),nAtoms,6)


       Call GETMEM('Coor','FREE','REAL',ipCoor,3*nSym*nAtoms)

      endif

      IF (ISLUSCUS .EQ. 1) THEN
        CALL PRTLUSENDGRID(LID)
        if(isUHF.eq.1) CALL PRTLUSENDGRID(LID_ab)
      END IF

      if(isSphere.eq.1) then
      Call GETMEM('SpDi','FREE','REAL',iSphrDist,nInc)
      endif
      if(isColor.eq.1) then
      Call GETMEM('SpCo','FREE','REAL',iSphrColor,nInc)
      endif
      Call GETMEM('Coor','FREE','REAL',ipC,nInc*3)

        Call GETMEM('DOValue','FREE','REAL',ipOut,nInc)
        Call GETMEM('MOValue','FREE','REAL',ipMO,nInc*nMOs)

      if(isUHF.eq.1) then
        Call GETMEM('CMO_ab' ,'FREE','REAL',ipCMO_ab,nCMO)
        Call GETMEM('Ener_ab','FREE','REAL',ipE_ab,nMOs)
        Call GETMEM('Occu_ab','FREE','REAL',ipOcc_ab,nMOs)
        Call GETMEM('Sort_ab','FREE','INTE',ipSort_ab,nMOs)
        Call GETMEM('NRef_ab','FREE','INTE',ipGRef_ab,nMOs)
        Call GETMEM('DoIt_ab','FREE','INTE',ipDoIt_ab,nMOs)
      endif
      if(isUserGrid.eq.1)
     *  Call GETMEM('Grid','FREE','REAL',ipGrid,nGridPoints*3)
      if (imoPack .ne. 0) then
        Call GETMEM('PackedBlock','FREE','INTE',ipPBlock,nInc)
      else
        Call GETMEM('PackedBlock','FREE','INTE',ipPBlock,1)
      endif

        Call GETMEM('Pab' ,'FREE','REAL',ipPab,nMOs)
        Call GETMEM('DoIt','FREE','INTE',ipDoIt,nMOs)

        Call GETMEM('NRef','FREE','INTE',ipGRef,nMOs)
        Call GETMEM('Nzer','FREE','INTE',ipNZ,nMOs*2)
        Call GETMEM('Sort','FREE','INTE',ipSort,nMOs)
        Call GETMEM('Vol' ,'FREE','REAL',ipVol,nMOs)
        Call GETMEM('iTyp','FREE','INTE',ipType,nMOs)
       if(isVirt.eq.1) then
        Call GETMEM('ddNo','FREE','REAL',ipdd,nMOs*nMOs)
       endif

        Call GETMEM('Occ2','FREE','REAL',ipOoo,nMOs)
        Call GETMEM('Occu','FREE','REAL',ipOcc,nMOs)
        Call GETMEM('Ener','FREE','REAL',ipE,nMOs)

c      if(isWDW.eq.1) call GETMEM('WDW','FREE','REAL',ipWdW,nCenter)
        Call GETMEM('CMO','FREE','REAL',ipCMO,nCMO)


      if (isAtom.eq.0.and.isXField.eq.0)
     &  Call GETMEM('Coor','FREE','REAL',ipCoor,3*nSym*nAtoms)

      if(isCutOff.eq.1)
     &  Call GETMEM('CUTFL','FREE','INTE',ipCutOff,nCoor)
*
************************************************************************

      Return
      End

      Subroutine save_ddNorm(ddNorm,ii,jj,Wdd,nMOs)
      Implicit Real*8 (A-H,O-Z)
      Dimension Wdd(nMOs,nMOs)
      Wdd(ii,jj)=Wdd(ii,jj)+ddNorm
      return
      end

      subroutine print_ddNorm(nMOs,Wdd,det3)
      Implicit Real*8 (A-H,O-Z)
      Dimension Wdd(nMOs,nMOs)
        do i=1,nMOs
        write (6,'(20F8.3)') (Wdd(i,j)*det3,j=1,nMOs)
        enddo
      return
      end
