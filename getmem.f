c
c  No, this is not a getmem!
c  This code simulates getmem in a very ugly way
c
c  (C) Valera Veryazov
c

      Subroutine GETMEM (NameIn,KeyIn,TypeIn,iPos,Length)
      include "WrkSpc.inc"
*
      Character*(*) NameIn,KeyIn,TypeIn
      logical Debug
      Debug=.false.

*----------------------------------------------------------------------*
*     Allocate new memory                                              *
*----------------------------------------------------------------------*

      If(KeyIn.eq.'ALLO') then
        if(TypeIn.eq.'INTE') then
          iPos=iPosiWORK
          iPosiWORK=iPosiWORK+Length
          iposci=iposci+1
          LastName(iposci)=Namein
          if(iPosiWORK.gt.IWORKLEN) goto 666
        endif
        if(TypeIn.eq.'REAL') then
          iPos=iPosWORK
          iPosWORK=iPosWORK+Length
          iposc=iposc+1
          LastName(iposc)=Namein
          if(iPosWORK.gt.IWORKLEN) goto 666
        endif     
      endif
      

      If(KeyIn.eq.'FREE') then
         if(TypeIn.eq.'INTE') then
           if(NameIn.eq.LastName(iposci)) then
             iPosiWORK=iPosiWORK-Length
            iposci=iposci-1
           else 
           if(Debug) write(6,*) 'Too be fixed:',NameIn,
     &   ' request free before ', LastName(iPosci)
           endif
         endif
         if(TypeIn.eq.'REAL') then
           if(NameIn.eq.LastName(iposc)) then
             iPosWORK=iPosWORK-Length
            iposc=iposc-1
           else 
           if(Debug) write(6,*) 'Too be fixed:',NameIn,
     &   ' request free before ', LastName(iPosc)
           endif
         endif
       endif

      If(KeyIn.eq.'CHEC') then
        write(6,*) NameIn
      endif
       return
666   write (6,*) 'Memory is gone..'
      End

      subroutine IniMem_
      include "WrkSpc.inc"
      iPosWORK=2
      iPosiWORK=2
      LastName=''
      ip_Dummy=1
      ip_iDummy=1
      iposc=0
      iposci=0
      return
      end
