          subroutine unpack_me(iout,iA,n)
         Dimension iA(n)
         Character*120 Str, StrT
         ind=1
1        read(iout,'(a)') Str
c         write(*,'(a)') Str
10       if(Str.eq.' ') goto 200
20       j=index(Str,' ')
           if(j.eq.1) then
             StrT=Str(2:)
             Str=StrT
             goto 20
           endif
         StrT=Str(j+1:)
         Str(j:)=' '
         k=index(Str,':')
         if(k.eq.0) then            
              read(Str,*) iA(ind)             
              ind=ind+1
         else
              read(Str(k+1:),*) iA(ind)
              read(Str(1:k-1),*) l
               do ll=1,l
                 iA(ind+ll)=iA(ind)
               enddo
               ind=ind+l
         endif
         Str=StrT
         goto 10
200      continue
c         print *,'VV',ind-1,n
         if(ind-1.lt.n) goto 1
         return
         end
                 

          subroutine unpack_meR(iout,A,n)
         IMPLICIT REAL*8 (A-H,O-Z)
         Dimension A(n)
         Character*120 Str, StrT
         ind=1
1        read(iout,'(a)') Str
c         write(*,'(a)') Str
10       if(Str.eq.' ') goto 200
20       j=index(Str,' ')
           if(j.eq.1) then
             StrT=Str(2:)
             Str=StrT
             goto 20
           endif
         StrT=Str(j+1:)
         Str(j:)=' '
         k=index(Str,':')
         if(k.eq.0) then            
              read(Str,*,err=100) A(ind)             
              ind=ind+1
         else
              read(Str(k+1:),*) A(ind)
              read(Str(1:k-1),*) l
               do ll=1,l
                 A(ind+ll)=A(ind)
               enddo
               ind=ind+l
         endif
         Str=StrT
         goto 10
200      continue
c         print *,'VV',ind-1,n
         if(ind-1.lt.n) goto 1
         return
100     write(6,*) 'error line:',Str
         end
       Subroutine push_iinfo(ia,ib,Len)
       Implicit Real*8 (A-H,O-Z)
        dimension ia(Len), ib(Len)
        do i=1,Len
        ib(i)=ia(i)
        enddo
        return
        end
        
       Subroutine push_rinfo(a,b,Len)
       Implicit Real*8 (A-H,O-Z)
        dimension a(Len), b(Len)
        do i=1,Len
        b(i)=a(i)
        enddo
        return
        end
        
          subroutine abend_q
          stop 0
          end

************************************************************************
      function mylen(s)
c
c  return real length of the string without spaces...
c
      Character*(*) s
      il=len(s)
      if(il.eq.0) then
      mylen=0
      return
      endif
      do i=il,1,-1
       if(s(i:i).ne.' ') then
        mylen=i
       return
       endif
      enddo
      mylen=0
      return
      end
************************************************************************
*                                                                      *
* This routine uppercases a text string.                               *
*                                                                      *
*----------------------------------------------------------------------*
*                                                                      *
* Author:  Per-AAke Malmqvist                                          *
*          Lund University                                             *
*                                                                      *
************************************************************************
      Subroutine UpCase(string)
      Character*(*)  string
      Character*26   up,lw
      Dimension      itab(0:255)
      Save           up,lw,ifset,itab

      Data up    /'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/
      Data lw    /'abcdefghijklmnopqrstuvwxyz'/
      Data ifset / 0 /

      If (ifset.eq.0) Then
        ifset=1
        Do i=0,255
          itab(i)=i
        End Do
        Do ii=1,26
          i=ichar(up(ii:ii))
          j=ichar(lw(ii:ii))
          itab(j)=i
        End Do
      End If

      Do ii=1,len(string)
        i=ichar(string(ii:ii))
        j=itab(i)
        string(ii:ii)=char(j)
      End Do

      Return
      End
      Function MyGetKey (InUnit, What, IValue, RValue, SValue,
     &    N, IArray, RArray)
************************************************************
*
*   <DOC>
*     <Name>MyGetKey</Name>
*     <Syntax>MyGetKey(InUnit,What,IValue,RValue,SValue,N,IArray,RArray)</Syntax>
*     <Arguments>
*       \Argument{InUnit}{Unit number}{Integer}{in}
*       \Argument{What}{Type of input}{Character*1}{inout}
*       \Argument{IValue}{Integer Value}{Integer}{out}
*       \Argument{RValue}{Real Value}{Real}{out}
*       \Argument{SValue}{String value}{Character*(*)}{out}
*       \Argument{N}{Size of array}{Integer}{in}
*       \Argument{IArray}{Integer Array}{Integer(N)}{out}
*       \Argument{RArray}{Real Array}{Real(N)}{out}
*     </Arguments>
*     <Purpose> General purpose routine to read arbitrary data from user input
*     </Purpose>
*     <Dependencies></Dependencies>
*     <Author>V.Veryazov</Author>
*     <Modified_by></Modified_by>
*     <Side_Effects></Side_Effects>
*     <Description>
*     The routine read a line from unit InUnit (ignoring molcas comments
*     and blank lines), and return a value or an array.
*
*     Parameter What specifies the type of data:
*     I - read an integer and return it as IValue
*     R - read a real and return it as RValue
*     S - read a string and return it as SValue
*     U - recognize integer/real/string and return corresponding value
*     A - read integer array and return IArray
*     D - read real array and return RArray
*
*     </Description>
*    </DOC>
*
************************************************************
      Implicit Real*8 (A-H,O-Z)
      character SValue *(*)
      Dimension IArray(*), RArray(*)
      character What
      character KWord*120
      MyGetKey=0
      iptr=1
      i=1
 1    Read(InUnit,'(A)',Err=20, end=20) KWord
      If (KWord(1:1).eq.'*'.or.KWord.eq.' ') Go To 1
      Call UpCase(KWord)
        if(What.eq.'I') then
              Read(KWord,*,Err=20) IValue
          else
           if(What.eq.'R') then
               Read(KWord,*,Err=20) RValue
             return
           else
           if(What.eq.'A') then
               Read(KWord,*,Err=20, End=40) (IArray(i),i=iptr,N)
           else

            if(What.eq.'D') then
               Read(KWord,*,Err=20, End=40) (RArray(i),i=iptr,N)
            else

            if(What.eq.'S') then
               Call NoBlanks(SValue, 120, KWord)
               goto 100
            else
             if(What.eq.'U') then
               Read(KWord,*,Err=2) IValue
               What='I'
               Goto 100
2              Read(KWord,*,Err=3) RValue
               What='R'
               Goto 100
3              Call NoBlanks(SValue, 120, KWord)
               What='S'
               Goto 100
             endif
            endif
           endif
          endif
         endif
        endif
100    return
40      iptr=i
        goto 1
20      MyGetKey=1
        return
       end
        subroutine NoBlanks(out,n,in)
        character out*(*), in*(n)
        integer flag
        flag=-1
        j=0
        do 1 i=1,len(in)
         if(flag.eq.-1.and.in(i:i).eq.' ') goto 1
         flag=0
         if(i.le.len(in)-1) then
           if(in(i:i+1).eq.'  ') goto 1
         endif
         j=j+1
         out(j:j)=in(i:i)
1       continue
        out(j+1:)=' '
        return
        end
      SUBROUTINE FZERO(B,N)
      INTEGER    N
      REAL*8     B(N)
      do i=1,n
       B(i)=0.0d0
      enddo
c      CALL DCOPY_(N,0.0D0,0,B,1)

      RETURN
      END

      SUBROUTINE IZERO(B,N)
      INTEGER    N
      INTEGER    B(N)

      do i=1,n
       B(i)=0
      enddo

c      CALL ICOPY(N,0,0,B,1)

      RETURN
      END


      SUBROUTINE FMOVE(IA,IB,N)
C      INTEGER    N
C      REAL*8     IA(N),IB(N)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION   IA(*),IB(*)

C      CALL DCOPY(N*RtoI,IA,1,IB,1)
      DO I=1,N
         IB(I)=IA(I)
      END DO

      RETURN
      END

      Subroutine RecPrt(Title,FmtIn,A,nRow,nCol)
      Implicit Real*8 (A-H,O-Z)
      Character*(*) Title
      Character*(*) FmtIn
      Dimension A(nRow,nCol)
c      Integer StrnLn
      Parameter (lPaper=120,lMaxTitle=60)
      Character*(lMaxTitle) Line
      Character*20 FMT
*----------------------------------------------------------------------*
      If (nRow*nCol.eq.0) Return
*----------------------------------------------------------------------*
*     print the title                                                  *
*----------------------------------------------------------------------*
      lTitle=1
      If ( lTitle.gt.0 ) then
         Do 10 i=1,lMaxTitle
             Line(i:i)=' '
10       Continue
         lLeft=1
         Do 20 i=lTitle,1,-1
            If ( Title(i:i).ne.' ' ) lLeft=i
20       Continue
         lLeft=lLeft-1
         Do 25 i=1,lMaxTitle
            If ( i+lLeft.le.lTitle ) Line(i:i)=Title(i+lLeft:i+lLeft)
25       Continue
         Write(6,*)
         Write(6,'(2X,A)') Line
c         Do 30 i=1,StrnLn(Line)
c            Line(i:i)='-'
c30       Continue
c         Write(6,'(2X,A)') Line
         Write(6,'(2X,A,I5,A,I5)') 'mat. size = ',nRow,'x',nCol
      End If
*----------------------------------------------------------------------*
*     determine the printing format                                    *
*----------------------------------------------------------------------*
c      lFmt=Strnln(FmtIn)
c      If ( lFmt.ne.0 ) then
c         FMT=FmtIn
c      Else
         Amax=A(1,1)
         Amin=A(1,1)
         Do 40 j=1,nCol
            Do 50 i=1,nRow
               Amax=Max(Amax,A(i,j))
               Amin=Min(Amin,A(i,j))
50          Continue
40       Continue
         Pmax=0.0D0
         If ( Abs(Amax).gt.1.0D-72 ) Pmax=Log10(Abs(Amax))
         iPmax=1+INT(Pmax)
         iPmax=Max(1,iPmax)
         Pmin=0.0D0
         If ( Abs(Amin).gt.1.0D-72 ) Pmin=Log10(Abs(Amin))
         iPmin=1+INT(Pmin)
         iPmin=Max(1,iPmin)
         nDigit=24
         nDecim=Min(16,nDigit-Max(iPmin,iPmax))
         nDecim=Max(nDecim,1)
         If ( Amax.lt.0.0D0 ) iPmax=iPmax+1
         If ( Amin.lt.0.0D0 ) iPmin=iPmin+1
         lNumbr=Max(iPmin,iPmax)+nDecim+2
         nCols=9
         lLine=nCols*lNumbr
         If ( lLine.gt.lPaper ) then
            If ( lLine.le.lPaper+nCols .and. nDecim.gt.1 ) then
               nDecim=nDecim-1
               lNumbr=Max(iPmin,iPmax)+nDecim
               lItem=Max(lNumbr,lPaper/nCols)
            Else
               nCols=5
               lItem=Max(lNumbr,lPaper/nCols)
            End If
         Else
            lItem=lNumbr
         End If
         Write(FMT,'(A,   I4.4,  A, I4.4,  A, I4.4,   A)')
     &             '(2X,',nCols,'F',lItem,'.',nDecim,')'
c      End if
*----------------------------------------------------------------------*
*     print the data                                                   *
*----------------------------------------------------------------------*
c      Write(6,*)
      Do 60 i=1,nRow
         Write(6,FMT)(A(i,j),j=1,nCol)
60    Continue
*----------------------------------------------------------------------*
*     End procedure                                                    *
*----------------------------------------------------------------------*
      Return
      End
