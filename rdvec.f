************************************************************************
      SUBROUTINE RDVEC(Name,LU_,LABEL,NSYM,NBAS,NORB,
     &   CMO, OCC, EORB, INDT,TITLE,iWarn,iErr)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION NBAS(NSYM),NORB(NSYM),CMO(*),OCC(*), INDT(*), EORB(*)
      CHARACTER*(*) TITLE, Name, Label
      Dimension vDum(2)
      Call RdVec_(Name,LU_,LABEL,0,NSYM,NBAS,NORB,
     &   CMO,vDum, OCC, vDum, EORB, vDum,
     &   INDT,TITLE,iWarn,iErr,iWFtype)
       RETURN
       END

      SUBROUTINE RDVEC_(Name,LU_,LABEL,IUHF,NSYM,NBAS,NORB,
     &   CMO,CMO_ab, OCC, OCC_ab, EORB, EORB_ab,
     &   INDT,TITLE,iWarn,iErr,iWFtype)
* --------------------------------------------------------------------------------
*  Advanced RdVec (to remove all clones!)
* --------------------------------------------------------------------------------
* iWFtype =  0  -- Unknown origin of orbitals
*            1  -- Orbitals for Guessorb
*            2  -- Orbitals for closed shell HF
*            3  -- Orbitals for closed shell DFT
*            4  -- Orbitals for unrestricted HF
*            5  -- Orbitals for unrestricted DFT
*            6  -- Natural orbitals for unrestricted HF
*            7  -- Natural orbitals for unrestricted DFT
*            8  --
* --------------------------------------------------------------------------------
      IMPLICIT REAL*8 (A-H,O-Z)
      include "WrkSpc.inc"
      DIMENSION NBAS(NSYM),NORB(NSYM),CMO(*),OCC(*), INDT(*), EORB(*)
      DIMENSION CMO_ab(*),OCC_ab(*), EORB_ab(*)
      CHARACTER*(*) TITLE, Name, Label
      CHARACTER LINE*256,FMT*40
c      LOGICAL Exist
      Character*7 Crypt, CryptUp
      Character*10 Buff
      Character*2 sDummy
*
* Note! the size of Magic must be exact (thanks to MS formatted inporb!)
*
      Character*8 Location
      data Crypt   /'fi123sd'/
      DATA CryptUP /'FIXXXSD'/
      include "inporbfmt.fh"
      Location='rdVec_'
      Line='not defined yet'
*
* Analyze Label
*
      iCMO=0
      iOCC=0
      iEne=0
      iInd=0
      iBeta=0
      If(index(Label,'C').ne.0) iCMO=1
      If(index(Label,'O').ne.0) iOcc=1
      If(index(Label,'E').ne.0) iEne=1
      If(index(Label,'I').ne.0) iInd=1
      If(index(Label,'A').ne.0) iBeta=-1
      If(index(Label,'B').ne.0) iBeta=1
*----------------------------------------------------------------------*
* Open file Name                                                       *
*----------------------------------------------------------------------*
      iErr=0
      Lu=Lu_
      open(Unit=Lu,file=Name)
c      If (.Not.Exist) Then
c        Write(6,*) 'RdVec: File ',Name(1:index(Name,' ')),' not found!'
c        Call abend_q()
c      End If
c      REWIND (LU)
*----------------------------------------------------------------------*
* Check version!                                                       *
*----------------------------------------------------------------------*
      READ(LU,'(A256)',END=999,ERR=999) Line
      iVer=0
      Do jVer=1,mxVer
        if(Magic(jVer).eq.Line(1:len(Magic(jVer)))) iVer=jVer
      End Do

      If(iVer.eq.0) Then
         write(6,*)Location,'INPORB file in old format'
         call abend_q
      End If
*----------------------------------------------------------------------*
* INFO section, read it unconditionally                                *
*----------------------------------------------------------------------*
50    READ(LU,'(A256)',END=999,ERR=999) Line
      If(Line(1:5).ne.'#INFO') goto 50
      Read(Lu,'(a)',end=999,err=999) Title
      Read(Lu,'(a)',End=999,Err=999) Line
      Line(76:80)='0 0 0'
      Read(Line,*) myiUHF,myNSYM,iWFtype
*     Read(Lu,*,end=999,err=999) myiUHF,myNSYM
* In case of UHF mismatch:
      If(myiUHF.ne.iUHF) Then
* Stop if UHF requested, but the INPORB is not UHF
        If(myiUHF.eq.0) then
         write(6,*) Location,Name,' IUHF does not match'
         call abend_q
        endif
* With a UHF INPORB, only go on if alpha or beta orbitals
* explicitly requested
        If(iUHF.eq.0.and.iBeta.eq.0)
     &    Call abend_q
c     SysAbendFileMsg(Location,Name,'IUHF does not match',' ')
      End If
      If(myNSYM.ne.NSYM) call abend_q
c      Call SysAbendFileMsg(Location,Name,
c     &   'NSYM does not match',' ')
      Call GETMEM('MYNBAS','ALLO','INTE',imyNBAS,NSYM)
      Call GETMEM('MYNORB','ALLO','INTE',imyNORB,NSYM)
      Read(Lu,*,end=999,err=999) (iWORK(imyNBAS+i-1),i=1,NSYM)
      Read(Lu,*,end=999,err=999) (iWORK(imyNORB+i-1),i=1,NSYM)
*----------------------------------------------------------------------*
* Do checks                                                            *
*----------------------------------------------------------------------*
        Do i=1,NSYM
          If(iWORK(imyNBAS+i-1).ne.NBAS(i)) Then
            Line='NBAS does not match'
            call abend_q
c            If(iWarn.eq.1) Then
c              Call SysWarnMsg(Location,Line,' ')
c            Else
c              Call SysAbendFileMsg(Location,Name,Line,' ')
c            End If
          End If
        End Do
      If(iWarn.gt.0) Then
        Do i=1,NSYM
          If(iWORK(imyNORB+i-1).lt.NORB(i)) Then
            Line='NORB does not match'
           call abend_q
c            If(iWarn.eq.1) Then
c              Call SysWarnMsg(Location,Line,' ')
c            Else
c              Call SysAbendFileMsg(Location,Name,Line,' ')
c            End If
          End If
        End Do
      End If
*----------------------------------------------------------------------*
* ORB section                                                          *
*----------------------------------------------------------------------*
      If(iCMO.eq.1) Then
        nDiv = nDivOrb(iVer)
        FMT = FmtOrb(iVer)
        Rewind(LU)
51      READ(LU,'(A256)',END=999,ERR=999) Line
        If(Line(1:4).ne.'#ORB') goto 51
        KCMO  = 0
        Do ISYM=1,NSYM
          Do IORB=1,iWORK(imyNORB+ISYM-1)
            Do IBAS=1,NBAS(ISYM),NDIV
              IBASEND=MIN(IBAS+NDIV-1,NBAS(ISYM))
111           READ(LU,'(A256)',END=999,ERR=999) LINE
              If(LINE(1:1).EQ.'*') GOTO 111
              If(iOrb.le.nOrb(iSym)) Then
                 READ(LINE,FMT,err=888,end=888)
     &               (CMO(I+KCMO),I=IBAS,IBASEND)
              End If
            End Do
            If(iOrb.le.nOrb(iSym)) KCMO=KCMO+NBAS(ISYM)
          End Do
        End Do
        If(iUHF.eq.1.or.iBeta.eq.1) Then
52        READ(LU,'(A256)',END=999,ERR=999) Line
          If(Line(1:5).ne.'#UORB') goto 52
          KCMO  = 0
          Do ISYM=1,NSYM
            Do IORB=1,iWORK(imyNORB+ISYM-1)
              Do IBAS=1,NBAS(ISYM),NDIV
                IBASEND=MIN(IBAS+NDIV-1,NBAS(ISYM))
112             READ(LU,'(A256)',END=999,ERR=999) LINE
                If(LINE(1:1).EQ.'*') GOTO 112
                If(iOrb.le.nOrb(iSym)) Then
                  If (iBeta.eq.1) Then
                    READ(LINE,FMT,err=888,end=888)
     &                  (CMO(I+KCMO),I=IBAS,IBASEND)
                  Else
                    READ(LINE,FMT,err=888,end=888)
     &                  (CMO_ab(I+KCMO),I=IBAS,IBASEND)
                  End If
                End If
              End Do
              If(iOrb.le.nOrb(iSym)) KCMO=KCMO+NBAS(ISYM)
            End Do
          End Do
        End If ! iUHF
      End If ! iCMO
*----------------------------------------------------------------------*
* OCC section                                                          *
*----------------------------------------------------------------------*
      If(iOcc.eq.1) Then
        nDiv = nDivOcc(iVer)
        FMT = FmtOcc(iVer)
        Rewind(LU)
53      READ(LU,'(A256)',END=999,ERR=999) Line
        If(Line(1:4).ne.'#OCC') goto 53
        KOCC  = 0
        Do ISYM=1,NSYM
          Do IORB=1,iWORK(imyNORB+ISYM-1),NDIV
            IORBEND=MIN(IORB+NDIV-1,iWORK(imyNORB+ISYM-1))
113         READ(LU,'(A256)',END=999,ERR=999) LINE
            If(LINE(1:1).EQ.'*') GOTO 113
            READ(LINE,FMT,err=888,end=888)
     &              (OCC(I+KOCC),I=IORB,IORBEND)
          End Do
*         KOCC=KOCC+iWORK(imyNORB+ISYM-1)
          KOCC=KOCC+nOrb(iSym)
        End Do
        If(iUHF.eq.1.or.iBeta.eq.1) Then
54        READ(LU,'(A256)',END=999,ERR=999) Line
          If(Line(1:5).ne.'#UOCC') goto 54
          KOCC=0
          Do ISYM=1,NSYM
            Do IORB=1,iWORK(imyNORB+ISYM-1),NDIV
              IORBEND=MIN(IORB+NDIV-1,iWORK(imyNORB+ISYM-1))
114           READ(LU,'(A256)',END=999,ERR=999) LINE
              If(LINE(1:1).EQ.'*') GOTO 114
              If (iBeta.eq.1) Then
                READ(LINE,FMT,err=888,end=888)
     &              (OCC(I+KOCC),I=IORB,IORBEND)
              Else
                READ(LINE,FMT,err=888,end=888)
     &              (OCC_ab(I+KOCC),I=IORB,IORBEND)
              End If
            End Do
*           KOCC=KOCC+iWORK(imyNORB+ISYM-1)
            KOCC=KOCC+nOrb(iSym)
          End Do
        End If ! iUHF
      End If ! iOCC
*----------------------------------------------------------------------*
* ONE section                                                          *
*----------------------------------------------------------------------*
      If(iEne.eq.1) Then
        nDiv = nDivEne(iVer)
        FMT = FmtEne(iVer)
        Rewind(LU)
55      READ(LU,'(A256)',END=666,ERR=666) Line
        If(Line(1:4).ne.'#ONE') goto 55
        KOCC  = 0
        Do ISYM=1,NSYM
          Do IORB=1,iWORK(imyNORB+ISYM-1),NDIV
            IORBEND=MIN(IORB+NDIV-1,iWORK(imyNORB+ISYM-1))
115         READ(LU,'(A256)',END=999,ERR=999) LINE
            If(LINE(1:1).EQ.'*') GOTO 115
            READ(LINE,FMT,err=888,end=888)
     &          (EORB(I+KOCC),I=IORB,IORBEND)
          End Do
*         KOCC=KOCC+iWORK(imyNORB+ISYM-1)
          KOCC=KOCC+nOrb(iSym)
        End Do
        If(iUHF.eq.1.or.iBeta.eq.1) Then
56        READ(LU,'(A256)',END=999,ERR=999) Line
          If(Line(1:5).ne.'#UONE') goto 56
          KOCC=0
          Do ISYM=1,NSYM
            Do IORB=1,iWORK(imyNORB+ISYM-1),NDIV
              IORBEND=MIN(IORB+NDIV-1,iWORK(imyNORB+ISYM-1))
116           READ(LU,'(A256)',END=999,ERR=999) LINE
              If(LINE(1:1).EQ.'*') GOTO 116
              If (iBeta.eq.1) Then
                READ(LINE,FMT,err=888,end=888)
     &              (EORB(I+KOCC),I=IORB,IORBEND)
              Else
                READ(LINE,FMT,err=888,end=888)
     &              (EORB_ab(I+KOCC),I=IORB,IORBEND)
              End If
            End Do
*           KOCC=KOCC+iWORK(imyNORB+ISYM-1)
            KOCC=KOCC+nOrb(iSym)
          End Do
        End If ! iUHF
      End If ! iOne
*----------------------------------------------------------------------*
* INDEX section                                                        *
*----------------------------------------------------------------------*
      If(iInd.eq.1) Then
        Rewind(LU)
57      READ(LU,'(A256)',END=666,ERR=666) Line
        If(Line(1:6).ne.'#INDEX') goto 57
        if(iVer.eq.iVer10) then
        FMT='(A4)'
        NDIV  = 4
        iShift=1
        Do ISYM=1,NSYM
c         iShift=(ISYM-1)*7
          Do IORB=1,iWORK(imyNORB+ISYM-1),NDIV
            READ(LU,FMT,err=666,end=666) Buff
            Do i=1,4
              IND=index(Crypt,Buff(i:i))+index(CryptUp,Buff(i:i))
              If(Buff(i:i).ne.' ') Then
                If(IND.eq.0) Then
                  WRITE(6,*) '* ERROR IN RDVEC WHILE READING TypeIndex'
                  WRITE(6,'(3A)') '* Type=',Buff(i:i), ' is UNKNOWN'
                  WRITE(6,*) '* TypeIndex information is IGNORED'
                  iErr=1
                  Close(Lu)
                  goto 777
                End If
                IndT(iShift)=IND
                iShift=iShift+1
              End If
            End Do
          End Do
        End Do
        endif ! Ver10
c Ver 11
        if(iVer.ge.iVer11) then
        FMT='(A4)'
        NDIV  = 10
        iShift=1
        Do ISYM=1,NSYM
c         iShift=(ISYM-1)*7
        read(LU,*)
          Do IORB=1,iWORK(imyNORB+ISYM-1),NDIV
            READ(LU,'(a2,A10)',err=666,end=666) sDummy,Buff
            Do i=1,10
              IND=index(Crypt,Buff(i:i))+index(CryptUp,Buff(i:i))
              If(Buff(i:i).ne.' ') Then
                If(IND.eq.0) Then
                  WRITE(6,*) '* ERROR IN RDVEC WHILE READING TypeIndex'
                  WRITE(6,'(3A)') '* Type=',Buff(i:i), ' is UNKNOWN'
                  WRITE(6,*) '* TypeIndex information is IGNORED'
                  iErr=1
                  Close(Lu)
                  goto 777
                End If
                IndT(iShift)=IND
                iShift=iShift+1
              End If
            End Do
          End Do
        End Do
        endif ! Ver10


        iA=1
        iB=1
        Do iSym=1,nSym
           Call iCopy(nOrb(iSym),IndT(iA),1,IndT(iB),1)
           iA=iA+nBas(iSym)
           iB=iB+nOrb(iSym)
        End Do
      End If  ! Index
      Close(Lu)
      Goto 777
*----------------------------------------------------------------------*
* a special case - INDEX information is not found                      *
*----------------------------------------------------------------------*
666   iErr=1
      WRITE(6,*) '* TypeIndex information is IGNORED *'
      Close(Lu)
*----------------------------------------------------------------------*
*                                                                      *
*----------------------------------------------------------------------*
777   Call GETMEM('MYNORB','FREE','INTE',imyNORB,NSYM)
      Call GETMEM('MYNBAS','FREE','INTE',imyNBAS,NSYM)
      Return
*----------------------------------------------------------------------*
*                                                                      *
*----------------------------------------------------------------------*
999   write(6,*) 'Error during reading INPORB',Line
       call abend_q
c      Call SysAbendFileMsg(Location,Name,
c     &   'Error during reading INPORB\n',Line)
888   write(6,*) 'Error during reading INPORB',Line
       call abend_q
c      Call SysAbendFileMsg(Location,Name,
c     &   'Error during reading INPORB\n',Line)
      End
************************************************************************
*                                                                      *
************************************************************************
*
      Subroutine Chk_vec_UHF(Name,Lu,isUHF)
c routine returns isUHF based on information in INPORB
      Character *(*) Name
      CHARACTER LINE*80,Location *11
c      Logical Exist
      include "inporbfmt.fh"
      Location='Chk_vec_UHF'
      Line='not defined yet'

      open(file=Name,unit=Lu)
c      If (.Not.Exist) Then
c       Write (6,*) 'RdVec: File ',Name(1:index(Name,' ')),' not found!'
c       Call Abend_q()
c      End If
c      REWIND (LU)
* Check version!
      READ(LU,'(A80)',END=999,ERR=999) Line
      iVer=0
      Do jVer=1,mxVer
        if(Magic(jVer).eq.Line(1:len(Magic(jVer)))) iVer=jVer
      End Do

      If(iVer.eq.0) Then
c          Call SysWarnMsg(Location,
c     & 'INPORB file in old format',' ')
        Call abend_q
       isUHF=0
       close(Lu)
       return
      End If
50    READ(LU,'(A80)',END=999,ERR=999) Line
      If(Line(1:5).ne.'#INFO') goto 50
* Now Do the real job
        Read (Lu,'(a)',end=999,err=999) Line
        Read(Lu,*,end=999,err=999) isUHF
        close(Lu)
      return
999   call abend_q
c      Call SysAbendFileMsg(Location,Name,
c     &     'Error during reading INPORB\n',Line)
      end
