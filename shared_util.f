      Subroutine AOEval(iAng,nCoor,Coor,xyz,RA,Transf,CffSph,nElem,
     *                  nCmp,
     &                  Angular,nTerm,nForm,Thr_Rad,nRad,mExp,nExp,
     &                  Alpha,Radial,nBas,CffCnt,AOValue,mAO,
     &                  px,py,pz,ipx,ipy,ipz)
************************************************************************
* Object: to compute the values of the AOs on a grid. The calculation  *
*         is sectioned in an angular part and a radial part. Due to the*
*         gradients we have 3 angular parts and 2 radial part.         *
*                                                                      *
*      Author:Roland Lindh, Dept. of Theoretical Chemistry, University *
*             of Lund, SWEDEN. November '95                            *
*      Modified by: Takashi Tsuchiya, Dept. of Theoretical Chemistry,  *
*                   University of Lund, SWEDEN. February 2004          *
************************************************************************
      Implicit Real*8 (a-h,o-z)
      Real*8 xyz(nCoor,3,0:iAng+nRad-1), Coor(3,nCoor), RA(3),
     &       CffSph(nElem,nCmp), Alpha(nExp), Radial(nCoor,nRad,nBas),
     &       CffCnt(mExp,nBas), AOValue(mAO,nCoor,nBas,nCmp)
      Integer Angular(nTerm,5,nForm)
      Logical Transf
*                                                                      *
************************************************************************
*                                                                      *
      Ind(iy,iz) = (iy+iz)*(iy+iz+1)/2 + iz + 1

c         Write(6,*) '********** AOEval ***********'
c         Write (6,*) 'In AOEval'
c         Call RecPrt('Coor',' ',Coor,3,nCoor)
c         Call RecPrt('CffCnt',' ',CffCnt,mExp,nBas)
c         Call RecPrt('CffSph',' ',CffSph,nElem,nCmp)
c         Write (6,*) 'RA=',RA

*                                                                      *
************************************************************************
*                                                                      *
*                                                                      *
************************************************************************
*                                                                      *
*---- Initialize AOValue
*AOValue(mAO,nCoor,nBas,nCmp)
           do i04=1,nCmp
         do i03=1,nBas
       do i02=1,nCoor
      do i01=1,mAO
         AOValue(i01,i02,i03,i04)=0.0D0
      enddo
      enddo
      enddo
      enddo
c      Call FZero(AOValue,mAO*nCoor*nBas*nCmp)
*
*---- Set the order of derivation
*
      nDrv=nRad-1
C     Write(6,*) '----- nDrv = ', nDrv
*                                                                      *
************************************************************************
*                                                                      *
*---- Evaluate the radial part
*----    Normal radial part and
*----    premultiplied with (minus two times the exponent)**iDrv
*
*
      Thre=-99D0
      If (Thr_Rad.gt.0.0D0) Thre=Log(Thr_Rad)
      Exp_Min=1.0D10
      Do iExp = 1, nExp
         Exp_Min=Min(Exp_Min,Alpha(iExp))
      End Do
cRadial(nCoor,nRad,nBas),
        do i03=1,nBas
       do i02=1,nRad
      do i01=1,nCoor
         Radial(i01,i02,i03)=0.0d0
      enddo
      enddo
      enddo
c      Call FZero(Radial,nCoor*nRad*nBas)
      Do iCoor = 1, nCoor
         R2=(Coor(1,iCoor)-RA(1))**2
     &     +(Coor(2,iCoor)-RA(2))**2
     &     +(Coor(3,iCoor)-RA(3))**2
c         print *,'R2',r2
C        If (-Exp_Min*R2.lt.Thre) Go To 9898
         Do iExp = 1, nExp
            If (-Alpha(iExp)*R2.lt.Thre) Go To 9898
            Tmp=Exp(-Alpha(iExp)*R2)
            If (nRad.eq.1) Then
               Do iBas = 1, nBas
                  XCff=CffCnt(iExp,iBas)
                  Radial(iCoor,1,iBas)=Radial(iCoor,1,iBas) + XCff*Tmp
               End Do
            Else If (nRad.eq.2) Then
               Tmp2=-2.0d0*Alpha(iExp)*Tmp
               Do iBas = 1, nBas
                  XCff=CffCnt(iExp,iBas)
                  Radial(iCoor,1,iBas)=Radial(iCoor,1,iBas) + XCff*Tmp
                  Radial(iCoor,2,iBas)=Radial(iCoor,2,iBas) + XCff*Tmp2
               End Do
            Else If (nRad.eq.3) Then
               Tmp2=-2.0d0*Alpha(iExp)*Tmp
               Tmp3=-2.0d0*Alpha(iExp)*Tmp2
               Do iBas = 1, nBas
                  XCff=CffCnt(iExp,iBas)
                  Radial(iCoor,1,iBas)=Radial(iCoor,1,iBas) + XCff*Tmp
                  Radial(iCoor,2,iBas)=Radial(iCoor,2,iBas) + XCff*Tmp2
                  Radial(iCoor,3,iBas)=Radial(iCoor,3,iBas) + XCff*Tmp3
               End Do
            Else If (nRad.eq.4) Then
               Tmp2=-2.0d0*Alpha(iExp)*Tmp
               Tmp3=-2.0d0*Alpha(iExp)*Tmp2
               Tmp4=-2.0d0*Alpha(iExp)*Tmp3
               Do iBas = 1, nBas
                  XCff=CffCnt(iExp,iBas)
                  Radial(iCoor,1,iBas)=Radial(iCoor,1,iBas) + XCff*Tmp
                  Radial(iCoor,2,iBas)=Radial(iCoor,2,iBas) + XCff*Tmp2
                  Radial(iCoor,3,iBas)=Radial(iCoor,3,iBas) + XCff*Tmp3
                  Radial(iCoor,4,iBas)=Radial(iCoor,4,iBas) + XCff*Tmp4
               End Do
            Else
               Do iBas = 1, nBas
                  XCff=CffCnt(iExp,iBas)
                  Radial(iCoor,1,iBas)=Radial(iCoor,1,iBas) + XCff*Tmp
               End Do
               Do iDrv = 1, nDrv
                  Tmp=-2.0d0*Alpha(iExp)*Tmp
                  Do iBas = 1, nBas
                     XCff=CffCnt(iExp,iBas)
                     Radial(iCoor,iDrv+1,iBas)=Radial(iCoor,iDrv+1,iBas)
     &                                        +  XCff*Tmp
                  End Do
               End Do
            End If
         End Do
 9898    Continue
      End Do
*
*                                                                      *
************************************************************************
*                                                                      *
*-----Evaluate the angular part
*
      If (iAng+nRad-1.ge.1) Then
         Do iCoor = 1, nCoor
            xyz(iCoor,1,0)=1.0d0
            xyz(iCoor,2,0)=1.0d0
            xyz(iCoor,3,0)=1.0d0
            xyz(iCoor,1,1)=px*(Coor(1,iCoor)-RA(1))
            xyz(iCoor,2,1)=py*(Coor(2,iCoor)-RA(2))
            xyz(iCoor,3,1)=pz*(Coor(3,iCoor)-RA(3))
         End Do
         Do i = 2, iAng+nRad-1
            Do iCoor = 1, nCoor
               xyz(iCoor,1,i)=xyz(iCoor,1,i-1)*xyz(iCoor,1,1)
               xyz(iCoor,2,i)=xyz(iCoor,2,i-1)*xyz(iCoor,2,1)
               xyz(iCoor,3,i)=xyz(iCoor,3,i-1)*xyz(iCoor,3,1)
            End Do
         End Do
      Else
         call dcopy_(3*nCoor,1.0d0,0,xyz(1,1,0),1)
      End If
c      print *,'xxx',xyz
*                                                                      *
************************************************************************
*                                                                      *
*-----Calculate the angular components of the derivatives
*
      Call ICopy(5*nForm*nTerm,0,0,Angular,1)
*
      Do ix = iAng, 0, -1
*
         Do iy = iAng-ix, 0, -1
            iz = iAng-ix-iy
            ip=Ind(iy,iz)
*
*---------- Initial values for 0th-order derivative
*
            Angular(1,1,1)=ix
            Angular(1,2,1)=iy
            Angular(1,3,1)=iz
            Angular(1,4,1)=0
            Angular(1,5,1)=1
*
*
*---------- Calculate derivatives of the angular components
*
*           jf: Formula to be derivated
*           if: New formula by way of derivation
*
            if = 1
C           Call PrntA(nterm,nform,Angular,if,0)
            Do jDrv = 0,nDrv-1
               Do jx = jDrv,0,-1
                  Do jy = jDrv-jx,0,-1
                     jz = jDrv-jx-jy
                     jf = Ind(jy,jz)
*
                     Do kDrv = 0,jDrv-1
                        jf=jf+(kDrv+1)*(kDrv+2)/2
                     End Do
*
*
                     If (jy .EQ. 0 .AND. jz .EQ. 0) then
                       if=if+1
                       Call dFdxyz(nTerm,nForm,Angular,jf,if,1,ipx,jDrv)
C                      Call PrntA(nterm,nform,Angular,if,jdrv+1)
                       if=if+1
                       Call dFdxyz(nTerm,nForm,Angular,jf,if,2,ipy,jDrv)
C                      Call PrntA(nterm,nform,Angular,if,jdrv+1)
                       if=if+1
                       Call dFdxyz(nTerm,nForm,Angular,jf,if,3,ipz,jDrv)
C                      Call PrntA(nterm,nform,Angular,if,jdrv+1)
                     Else if (jz .EQ. 0) then
                       if=if+1
                       Call dFdxyz(nTerm,nForm,Angular,jf,if,2,ipy,jDrv)
C                      Call PrntA(nterm,nform,Angular,if,jdrv+1)
                       if=if+1
                       Call dFdxyz(nTerm,nForm,Angular,jf,if,3,ipz,jDrv)
C                      Call PrntA(nterm,nform,Angular,if,jdrv+1)
                     Else
                       if=if+1
                       Call dFdxyz(nTerm,nForm,Angular,jf,if,3,ipz,jDrv)
C                      Call PrntA(nterm,nform,Angular,if,jdrv+1)
                     Endif
                  End Do
               End Do
            End Do
*
*
*---------- Distribute contributions to the real spherical harmonics
*----------            and combine with angular part
*----------       to yield the values and the gradients
*
            If (Transf) Then
              Do iCmp = 1, nCmp
                Cff=CffSph(ip,iCmp)
c            print *,'cff',ip,icmp,CffSph(ip,iCmp)
                If (Cff.ne.0.0d0) Then
                  kForm=0
                  Do iDrv = 0,nDrv
                    mForm=(iDrv+1)*(iDrv+2)/2
                    Do iForm = kForm+1, kForm+mForm
                      mTerm=2**(iDrv)
                      Do iTerm = 1, mTerm
                        iCoef=Angular(iTerm,5,iForm)
                        If (iCoef.ne.0) Then
                          Coef=DBLE(iCoef)
                          iRad =Angular(iTerm,4,iForm)+1
                          Do iBas = 1, nBas
                            Do iCoor = 1, nCoor
                              AOValue(iForm,iCoor,iBas,iCmp)
     &                         = AOValue(iForm,iCoor,iBas,iCmp)
     &                         + xyz(iCoor,1,Angular(iTerm,1,iForm))
     &                         * xyz(iCoor,2,Angular(iTerm,2,iForm))
     &                         * xyz(iCoor,3,Angular(iTerm,3,iForm))
     &                         * Coef
     &                         * Cff
     &                         * Radial(iCoor,iRad,iBas)
c                         print *,'AOt',AOValue(iForm,iCoor,iBas,ip),
c     *                          coef,cff,Radial(iCoor,iRad,iBas)
                            End Do
                          End Do
                        End If
                      End Do
                    End Do
                    kForm=kForm+mForm
                  End Do
                End If
              End Do
            Else
              kForm=0
              Do iDrv = 0, nDrv
                mForm=(iDrv+1)*(iDrv+2)/2
                Do iForm = kForm+1, kForm+mForm
                  mTerm=2**(iDrv)
                  Do iTerm = 1, mTerm
                    iCoef=Angular(iTerm,5,iForm)
                    If (iCoef.ne.0) Then
                      Coef=DBLE(iCoef)
                      iRad =Angular(iTerm,4,iForm)+1
                      Do iBas = 1, nBas
                        Do iCoor = 1, nCoor
                          AOValue(iForm,iCoor,iBas,ip)
     &                     = AOValue(iForm,iCoor,iBas,ip)
     &                     + xyz(iCoor,1,Angular(iTerm,1,iForm))
     &                     * xyz(iCoor,2,Angular(iTerm,2,iForm))
     &                     * xyz(iCoor,3,Angular(iTerm,3,iForm))
     &                     * Coef
     &                     * Radial(iCoor,iRad,iBas)
c                         print *,'AOv',AOValue(iForm,iCoor,iBas,ip)
                        End Do
                      End Do
                    End If
                  End Do
                End Do
                kForm=kForm+mForm
              End Do
            End If
*
*
         End Do
*
      End Do
*                                                                      *
************************************************************************
*                                                                      *
*
*     Call GETMEM('AOEval ','CHEC','REAL',iDum,iDum)
      Return
      End
*
      Subroutine dFdxyz(mterm,mform,N,jp,ip,ixyz,ipf,jdrv)
*
      Implicit real*8 (a-h,o-z)
      Integer N(mterm,5,mform)
*
*
*     ipf: Phase factor in integer
*
*
C     Write(6,*) '--- Start dFdxyz ---'
C     Write(6,9000) 'jp',jp
C     Write(6,9000) 'ip',ip
C     Write(6,9000) 'ixyz',ixyz
C     Write(6,9000) 'ipf',ipf
C     Write(6,9000) 'jdrv',jdrv
*
*
      nterm=2**jdrv
      iterm=0
*
      Do jterm=1,nterm
*
*
*        downward operation
*        (derivation of angular part)
*
*
         iterm=iterm+1
         Do i=1,5
            If (i .EQ. ixyz) then
               N(iterm,ixyz,ip)=N(jterm,ixyz,jp)-1
            Else
               N(iterm,i,ip)=N(jterm,i,jp)
            End if
         End do
         N(iterm,5,ip)=N(iterm,5,ip)*N(jterm,ixyz,jp)
         N(iterm,5,ip)=N(iterm,5,ip)*ipf
*
*
*        upward operation
*        (derivation of radial  part)
*
*
         iterm=iterm+1
         Do i=1,5
            If (i .EQ. ixyz) then
               N(iterm,ixyz,ip)=N(jterm,ixyz,jp)+1
            Else
               N(iterm,i,ip)=N(jterm,i,jp)
            End if
         End do
         N(iterm,4,ip)=N(iterm,4,ip)+1
         N(iterm,5,ip)=N(iterm,5,ip)*ipf
      End do
*
*
*
      Return
      End
************************************************************************
*                                                                      *
* Copyright (C) 1995, Roland Lindh                                     *
*               2000, Valera Veryazov                                  *
*               2014, Thomas Dresselhaus                               *
************************************************************************
      Subroutine MOEval(MOValue,nMOs,nCoor,CCoor,CMOs,nCMO,DoIt,
     &                  nDrv,mAO)
************************************************************************
*                                                                      *
* Object:                                                              *
*                                                                      *
*                                                                      *
*      Author:Roland Lindh, Dept. of Theoretical Chemistry, University *
*             of Lund, SWEDEN. November 1995                           *
*                                                                      *
*      Modified: Thomas Dresselhaus, March 2014                        *
*                Added ability to calculate 2nd derivative as well     *
************************************************************************
c      use Real_Spherical
      Implicit Real*8 (A-H,O-Z)
       include "info.fh"
       include "WrkSpc.inc"
*comdeck sphere.inc $Revision: 7.8 $
      Integer ipSph(0:MxAng)
      Common /Sph/ ipSph

      Real*8 A(3),Ccoor(3,nCoor),RA(3)
      Integer DoIt(nMOs)
      Integer nDrv ! Between 0 and 2. The highest derivative to be calc.
      Integer mAO  ! Memory slots per point and basis functions. Should
                   ! be >=1 for nDrv=0, >=4 for nDrv=1, >=10 for nDrv=2.
      Real*8 MOValue(mAO,nCoor,nMOs),CMOs(nCMO)
*
*     Statement functions
*
      nElem(ixyz) = (ixyz+1)*(ixyz+2)/2
      IndSOff(iCnttp,iCnt)=(iCnttp-1)*Max_Cnt+iCnt
*
*
c         Write (6,*) ' In MOEval'
      call dcopy_(mAO*nCoor*nMOs,0.0d0,0,MOValue,1)
*
*     Loop over shells.
*
      iSkal=0
c      print *,' iAngMx', iAngMx
      Thr=0.0D0

      Do iAng = iAngMx , 0, -1

c      print *,' maxprm', MaxPrm(iAng)
c      print *,' maxbas', Maxbas(iAng)

         If (MaxPrm(iAng).eq.0) goto 100
         If (MaxBas(iAng).eq.0) goto 100
*
*        Scratch area for contraction step
*
         nScr1 =  MaxPrm(iAng)* nElem(iAng)
c         print *,'nScr1',nScr1
         Call GETMEM('Scrtch','ALLO','REAL',iScrt1,nScr1)
*
*        Scratch area for the transformation to spherical gaussians
*
         nScr2=MaxPrm(iAng)*nElem(iAng)
         Call GETMEM('ScrSph','ALLO','REAL',iScrt2,nScr2)
*
*        Loop over basis sets. Skip if basis set do not include
*        angular momentum functions as specified above.
*
         iAOttp=0
         mdc = 0
c       print *,' nCnttp', nCnttp

         Do iCnttp = 1, nCnttp

            nTest = nVal_Shells(iCnttp)
            If (iAng+1.gt.nTest)  then
c              print *, 'goto iang',iang+1,nTest
                 Go To 101
            endif
           If (AuxCnttp(iCnttp)) then
c              print *, 'goto aux',AuxCnttp(iCnttp)
              Go To 101
            endif
c            If (FragCnttp(iCnttp)) then
c              print *, 'goto frag',FragCnttp(iCnttp)
c              Go To 101
c            endif
c           Write (*,*) ' iCnttp=',iCnttp
            nCnt = nCntr(iCnttp)
            iShll = ipVal(iCnttp) + iAng
            iExp=ipExp(iShll)
            iCff=ipCff(iShll)
            iPrim = nExp(iShll)
            If (iPrim.eq.0) Go To 101
            iBas  = nBasis(iShll)
            If (iBas.eq.0) Go To 101
            iCmp  = nElem(iAng)
            If (Prjct(iShll)) iCmp = 2*iAng+1
            Call OrdExpD2C(iPrim,WORK(iExp),iBas,WORK(iCff))
*
*           Loop over unique centers of basis set "iCnttp"
*
            IncAO=lOffAO(iCnttp)

            Do iCnt = 1, nCnt

               ixyz = ipCntr(iCnttp) + (iCnt-1)*3
               iAO = iAOttp + (iCnt-1)*IncAO + kOffAO(iCnttp,iAng)
               iShell = Ind_Shell(IndSOff(iCnttp,iCnt)) + iAng + 1
c           print *,'vv',ixyz,iAO,iShell
c        print *,'xx',Work(ixyz+1)

               call dcopy_(3,WORK(ixyz),1,A,1)
*
*--------------Allocate memory for SO and AO values
*
               mRad     = nDrv + 1

               nForm    = 0
               Do iDrv  = 0, nDrv
                 nForm = nForm + nElem(iDrv)
               End Do
               nTerm    = 2**nDrv

c            print *,'VV',mdc,iCnt,nStab(mdc+iCnt)
               nAO=(iCmp*iBas*nCoor)*(mAO)
               nSO=nAO*nIrrep/nStab(mdc+iCnt)
               nDeg=nIrrep/nStab(mdc+iCnt)
               Call GETMEM('AOs','ALLO','REAL',ipAOs,nAO)
               Call GETMEM('SOs','ALLO','REAL',ipSOs,nSO)
               call dcopy_(nSO,0.0d0,0,WORK(ipSOs),1)
               nxyz=nCoor*3*(iAng+mRad)
               Call GETMEM('xyz','ALLO','REAL',ipxyz,nxyz)
               ntmp=nCoor
               Call GETMEM('tmp','ALLO','REAL',iptmp,ntmp)
               nRadial=iBas*nCoor*mRad
               Call GETMEM('Radial','ALLO','REAL',ipRadial,nRadial)

               nAngular=5*nForm*nTerm
               Call GETMEM('Angular','ALLO','INTE',ipAng,nAngular)
*
*------------- Loops over symmetry operations operating on the basis
*              set center.
*
               Do iG = 0, nIrrep/nStab(mdc+iCnt) - 1
                  iSkal=iSkal+1
*                 Write (*,*) 'iSkal=',iSkal
                  ipx=iPhase(1,iCoSet(iG,0,mdc+iCnt))
                  ipy=iPhase(2,iCoSet(iG,0,mdc+iCnt))
                  ipz=iPhase(3,iCoSet(iG,0,mdc+iCnt))
                  px=DBLE(iPhase(1,iCoSet(iG,0,mdc+iCnt)))
                  py=DBLE(iPhase(2,iCoSet(iG,0,mdc+iCnt)))
                  pz=DBLE(iPhase(3,iCoSet(iG,0,mdc+iCnt)))
                  RA(1)  = px*A(1)
                  RA(2)  = py*A(2)
                  RA(3)  = pz*A(3)
c                   print *,'ra',RA
                  nOp = NrOpr(iCoSet(iG,0,mdc+iCnt),iOper,nIrrep)
*
*---------------- Evaluate AOs at RA
*
                  call dcopy_(nAO,0.0d0,0,WORK(ipAOs),1)
                  mTmp=1
                  Call AOEval(iAng,nCoor,CCoor,WORK(ipxyz),RA,
     &                        Transf(iShll),
     &                        WORK(ipRph+ipsph(iang)),nElem(iAng),iCmp,
     &                        iWORK(ipAng),nTerm,nForm,Thr,mRad,
     &                        iPrim,iPrim,WORK(iExp),
     &                        WORK(ipRadial),iBas,WORK(iCff),
     &                        WORK(ipAOs),mAO,px,py,pz,ipx,ipy,ipz)
*
*---------------- Distribute contributions to the SOs
*
                  Call SOAdpt(WORK(ipAOs),mAO,nCoor,iBas,iCmp,nOp,
     &                        WORK(ipSOs),nDeg,iShell)
*
               End Do
*
*------------- Distribute contributions to the MOs
*
c                print *,'sos',(work(ipsos+jjj),jjj=1,4)
               Call SODist(WORK(ipSOs),mAO,nCoor,iBas,iCmp,nDeg,
     &                     MOValue,iShell,nMOs,iAO,CMOs,nCMO,DoIt)
*
               Call GETMEM('Angular','FREE','INTE',ipAng,nAngular)
               Call GETMEM('Radial','FREE','REAL',ipRadial,nRadial)
               Call GETMEM('tmp','FREE','REAL',iptmp,ntmp)
               Call GETMEM('xyz','FREE','REAL',ipxyz,nxyz)
               Call GETMEM('SOs','FREE','REAL',ipSOs,nSO)
               Call GETMEM('AOs','FREE','REAL',ipAOs,nAO)
*
            End Do
 101        Continue
            mdc = mdc + nCntr(iCnttp)
            iAOttp = iAOttp + lOffAO(iCnttp)*nCntr(iCnttp)
         End Do
         Call GETMEM('ScrSph','FREE','REAL',iScrt2,nScr2)
         Call GETMEM('Scrtch','FREE','REAL',iScrt1,nScr1)
 100     continue
      End Do
*
*     Call GETMEM('MOEval_E ','CHEC','REAL',iDum,iDum)
      Return
      End
************************************************************************
      Integer Function NrOpr(iOp,iOper,nIrrep)
************************************************************************
*                                                                      *
* Object: to return the order index of a symmetry operator.            *
*                                                                      *
************************************************************************
      Integer iOper(0:nIrrep-1)
      NrOpr=-1
      Do 10 iIrrep = 0, nIrrep - 1
         If (iOp.eq.iOper(iIrrep)) NrOpr=iIrrep
 10   Continue
      Return
      End
      SubRoutine OrdExpD2C(nExp,Exp,nCntrc,Cff)
      Implicit Real*8 (A-H,O-Z)
      Real*8 Exp(nExp), Cff(nExp,nCntrc)
*
*     Order exponents diffuse to compact
*     Make the subsequent change in the contraction
*     matrix
*
c       print *,'Exp',Exp
      Do iExp = 1, nExp-1
         Exp1 = Exp(iExp)
         kExp = iExp
         Do jExp = iExp+1, nExp
            Exp2 = Exp(jExp)
            If (Exp2.lt.Exp1) Then
               Exp1 = Exp2
               kExp = jExp
            End If
         End Do
         If (kExp.ne.iExp) Then
c       print *,'swapping'
            Call DSwap_(1,Exp(iExp),1,Exp(kExp),1)
            Call DSwap_(nCntrc,Cff(iExp,1),nExp,Cff(kExp,1),nExp)
         End If
      End Do
*
      Return
      End
      Subroutine SOAdpt(AOValue,mAO,nCoor,mBas,
     &                  nCmp,nOp,SOValue,nDeg,iShell)
      Implicit Real*8 (a-h,o-z)
      include "info.fh"
      Real*8 AOValue(mAO,nCoor,mBas,nCmp),
     &       SOValue(mAO,nCoor,mBas,nCmp*nDeg), Aux(8)
      Integer   iTwoj(0:7)
      Data iTwoj/1,2,4,8,16,32,64,128/
*
*     Call GETMEM('SOAdpt_E','CHEC','REAL',iDum,iDum)
*
c       MolWgh=2
c      If (MolWgh.eq.0) Then
c         Fact=1.0d0/DBLE(nDeg)
c      Else If (MolWgh.eq.1) Then
c         Fact=1.0d0
c      Else
         Fact=1.0d0/Sqrt(DBLE(nDeg))
c      End If
      iSO=1
      Do i1 = 1, nCmp
         iaux=0
         Do j1 = 0, nIrrep-1
            If (iAnd(IrrCmp(IndS(iShell)+i1),iTwoj(j1)).eq.0) goto 100
            iaux=iaux+1
            xa= rChTbl(j1,nOp)
            Aux(iAux)=Fact*xa
 100     Continue
         End Do
c        print *,'AO',AOvalue
         Call DnaXpY(iAux,mAO*nCoor*mBas,Aux,1,
     &               AOValue(1,1,1,i1),1,0,
     &               SOValue(1,1,1,iSO),1,mAO*nCoor*mBas)
         iSO=iSO+iAux
      End Do
*
c      print *,'so',sovalue
*
*     Call GETMEM('SOAdpt_X','CHEC','REAL',iDum,iDum)
      Return
      End
      Subroutine SODist(SOValue,mAO,nCoor,mBas,nCmp,
     &                  nDeg,MOValue,iShell,
     &                  nMOs,iAO,CMOs,nCMO,DoIt)
      Implicit Real*8 (a-h,o-z)
      include "info.fh"
      Real*8 SOValue(mAO*nCoor,mBas,nCmp*nDeg),
     &       MOValue(mAO*nCoor,nMOs),
     &       CMOs(nCMO)
      Integer DoIt(nMOs)
      Integer   iOff_MO(0:7), iOff_CMO(0:7), iTwoj(0:7)
      Data iTwoj/1,2,4,8,16,32,64,128/
*
*
      itmp1=1
      itmp2=0
      Do iIrrep = 0, nIrrep-1
         iOff_MO(iIrrep)=itmp1
         iOff_CMO(iIrrep)=itmp2
         itmp1=itmp1+nBas(iIrrep)
         itmp2=itmp2+nBas(iIrrep)*nBas(iIrrep)
      End Do
*

      Do i1 = 1, nCmp
         iDeg=0
         Do iIrrep = 0, nIrrep-1
            If (iAnd(IrrCmp(IndS(iShell)+i1),iTwoj(iIrrep)).eq.0)
     &         goto 100
            iDeg=iDeg+1
            iSO=iAOtSO(iAO+i1,iIrrep)
            iOff=(i1-1)*nDeg+iDeg

*
*---------- Distribute contribution to all MO's in this irrep
*
            iMO=iOff_MO(iIrrep)
            iCMO=iOff_CMO(iIrrep)+iSO
            Call MyDGeMM(DoIt(iMO),
     &                 mAO*nCoor,nBas(iIrrep),mBas,
     &                 SOValue(1,1,iOff),mAO*nCoor,
     &                 CMOs(iCMO),nBas(iIrrep),
     &                 MOValue(1,iMO),mAO*nCoor)
 100     continue
          End Do
      End Do
*
c      print *, 'sov', SOvalue
      Return
      End

      SUBROUTINE MYDGEMM ( DoIt, M, N, K,
     $                     A, LDA, B, LDB,
     $                     C, LDC )
*     .. Scalar Arguments ..
      INTEGER            M, N, K, LDA, LDB, LDC
      Integer DoIt(*)
*     .. Array Arguments ..
      REAL*8   A( LDA, * ), B( LDB, * ), C( LDC, * )
*     ..
*  Purpose
*  =======
*
*  DGEMM for a spesial case.
*
*
*     .. Local Scalars ..
      INTEGER            I, J, L
      REAL*8   TEMP
*     .. Parameters ..
      REAL*8   ZERO
      PARAMETER        ( ZERO = 0.0D+0 )
*     ..
*
*           Form  C := A*B + C.
*
            DO 90, J = 1, N
            if(DoIt(J).eq.1) then
               DO 80, L = 1, K
                  IF( B( L, J ).NE.ZERO )THEN
                     TEMP = B( L, J )
                     DO 70, I = 1, M
                        C( I, J ) = C( I, J ) + TEMP*A( I, L )
   70                CONTINUE
                  END IF
   80          CONTINUE
            endif
   90       CONTINUE
*
      RETURN
      END
*
      Subroutine MyCoor(INPORB,iRun,isAuto,
     &      Ox,Oy,Oz, Rx,Ry,Rz, iGx,iGy,iGz, iMagic,iCustOrig)
************************************************************************
*                                                                      *
*   Read Coordinates and calculate a cub for grid                      *
*   isAuto=1 - real job, else only print                               *
*   Origin(3) fix cub in space                                         *
*   Rx,Ry,Rz - size of cub                                             *
*   iGx,iGy,iGz - net                                                  *
*   iMagic = magic guess for net                                       *
************************************************************************
      Implicit Real*8 (A-H,O-Z)
       Integer LenIn
       parameter (LenIn=6)

       include "WrkSpc.inc"
       include "grid.fh"
      Character*(LENIN) Byte4
      Character*128 Line
      Character*(*) INPORB
cVV: the constant is used in all GV packages
         R529=0.52917721067d0
*----------------------------------------------------------------------*
*     Prologue                                                         *
*----------------------------------------------------------------------*
       if(iRun.gt. 100) write(6,*) iRun
       lw2=1

       open(unit=26,file=INPORB)
1       read(26,'(a)',err=10,end=10) Line
       if(Line(1:6).eq.'#COORD') goto 20
       goto 1
10     Write(6,*) 'File ',INPORB, ' is missing COORD section'
       call abend_q
20     continue
      read(26,*) nAtoms
      Call GETMEM('Coor','ALLO','REAL',ipCoor,3*nAtoms)
      read(26,*) 
      irr=0
      do i=0,nAtoms-1
      
      read(26,'(1x,A2,2x,3F15.8)') AtomLbl(i+1), x,y,z
      WORK(ipCoor+irr)=x/R529
      irr=irr+1
      WORK(ipCoor+irr)=y/R529
      irr=irr+1     
      WORK(ipCoor+irr)=z/R529
      irr=irr+1
       enddo
      

      close(26)
      nCenter=nAtoms
* Check : are any strange names?
      NoOrig=0
      Do iAt=0,nCenter-1
       write(line,'(a)') AtomLbl(lw2+iAt)
       if(index( line,'Ori').ne.0) NoOrig=NoOrig+1
      enddo


*
      IF (ISLUSCUS .EQ. 1 .AND. ISBINARY .EQ. 3) THEN
        WRITE(LINE,'(2X,I8)') NCENTER-NOORIG
        CALL PRINTLINE(LID, LINE, 10,0)
        CALL PRINTLINE(LID, LINE, 0,0)
        if(isUHF.eq.1) then
        CALL PRINTLINE(LID_ab, LINE, 10,0)
        CALL PRINTLINE(LID_ab, LINE, 0,0)
        endif
        DO IAT=0,NCENTER-1
          WRITE(LINE,'(A)') ATOMLBL(LW2+IAT)
          IF(INDEX( LINE,'ORI').EQ.0) THEN
          Byte4=ATOMLBL(LW2+IAT)(1:2)
          if(index ('0123456789',Byte4(2:2)).ne.0) Byte4(2:2)=' '
            WRITE(LINE,'(1X,A2,2X,3F15.8)') Byte4,
     &  WORK(ipCoor+3*iAt)*r529,WORK(ipCoor+3*iAt+1)*r529,
     &  WORK(ipCoor+3*iAt+2)*r529
            CALL PRINTLINE(LID, LINE, 50,0)
            if(isUHF.eq.1) then
            CALL PRINTLINE(LID_ab, LINE, 50,0)
            endif
          ENDIF
        ENDDO
      END IF

      if(iCustOrig.eq.1) goto 500

      if(isAuto.eq.1) Then
*----------------------------------------------------------------------*
*     Find Cub parameters                                              *
*                 Ox->RxMin, Rx->RxMax
*----------------------------------------------------------------------*
      Ox=9999.9D0
      Oy=9999.9D0
      Oz=9999.9D0
      Rx=-9999.9D0
      Ry=-9999.9D0
      Rz=-9999.9D0

      Do iAt=0,nCenter-1
       rrx=WORK(ipCoor+3*iAt)
       rry=WORK(ipCoor+3*iAt+1)
       rrz=WORK(ipCoor+3*iAt+2)
       if(rrx.lt.Ox) Ox=rrx
       if(rrx.gt.Rx) Rx=rrx
       if(rry.lt.Oy) Oy=rry
       if(rry.gt.Ry) Ry=rry
       if(rrz.lt.Oz) Oz=rrz
       if(rrz.gt.Rz) Rz=rrz
      End Do
       Rx=Rx-Ox
       Ry=Ry-Oy
       Rz=Rz-Oz
*
* and now, expand this cub to place all atoms inside
*
      Ox=dble(int(Ox-TheGap))
      Oy=dble(int(Oy-TheGap))
      Oz=dble(int(Oz-TheGap))
      Rx=dble(int(Rx+2.0d0*TheGap))
      Ry=dble(int(Ry+2.0d0*TheGap))
      Rz=dble(int(Rz+2.0d0*TheGap))
* finish of iAuto.
      endif
*----------------------------------------------------------------------*
*     Calculate corrected coords
*----------------------------------------------------------------------*
*
* make a stupid Patch: Cerius2 WORKs well only with even nets!
*
       Rx=dble(int(Rx)/2*2)
       Ry=dble(int(Ry)/2*2)
       Rz=dble(int(Rz)/2*2)

       if(iMagic.gt.0) then
           iGx=int(abs(Rx))*iMagic
           iGy=int(abs(Ry))*iMagic
           iGz=int(abs(Rz))*iMagic
        endif
*
* make a stupid Patch: Cerius2 WORKs well only with even nets!
*
      iGx=(iGx+1)/2*2
      iGy=(iGy+1)/2*2
      iGz=(iGz+1)/2*2
      mynCenter=nCenter


500   continue
      Write(6,*)
      Write(6,'(6X,A)')'Cartesian coordinates:'
      Write(6,'(6X,A)')'-----------------------------------------'
      Write(6,'(6X,A)')'No.  Label     X         Y         Z     '
      Write(6,'(6X,A)')'-----------------------------------------'
      Do iAt=0,nCenter-1
        Write(6,'(4X,I4,3X,A,2X,3F10.5)')
     &  iAt+1,AtomLbl(lw2+iAt),
     &  WORK(ipCoor+3*iAt)*r529,WORK(ipCoor+3*iAt+1)*r529,
     &  WORK(ipCoor+3*iAt+2)*r529
      End Do
      Write(6,'(6X,A)')'-----------------------------------------'
      Write(6,'(6X,A,3F12.6)')'Grid Origin      = ',Ox,Oy,Oz
      Write(6,'(6X,A,3F12.6)')'Grid Axis Length = ',Rx,Ry,Rz
      Write(6,'(6X,A)')'-----------------------------------------'
      Write(6,*)
      Write(6,*)

      Call GETMEM('Coor','FREE','REAL',ipCoor,3*nSym*nAtoms)
*
*----------------------------------------------------------------------*
*     Normal exit                                                      *
*----------------------------------------------------------------------*
      Return
      End
*----------------------------------------------------------------------*
